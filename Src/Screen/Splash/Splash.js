import React, { Component } from 'react';
import { View, Text, ImageBackground, Image, Animated, ProgressBarAndroid } from 'react-native';
import { Container } from 'native-base';
import { responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions';

class Splash extends Component {
  constructor(props) {
    super(props);
    this.state = {
      logoAnime: new Animated.Value(0),
      logoText: new Animated.Value(0),
      loadingSpinner: false
    };
  }

  componentDidMount() {
    const { logoAnime, logoText } = this.state
    Animated.parallel([
      Animated.spring(logoAnime, {
        toValue: 1,
        tension: 10,
        friction: 2,
        duration: 1000
      }).start(),

      Animated.timing(logoText, {
        toValue: 1,
        duration: 2000
      })
    ]).start(() => { this.setState({ loadingSpinner: true }) })
  }

  render() {
    return (
      <Container>
        <ImageBackground style={{ flex: 1, alignItems: 'center', justifyContent: 'center', }}
          source={require('../../components/Images/BG/SplashBG.jpg')}>
          <View style={{flex:0.8,alignItems: 'center',justifyContent: 'center',paddingTop:responsiveWidth(10)}}>
          <Animated.View style={{ opacity: this.state.logoAnime, top: this.state.logoAnime.interpolate({ inputRange: [0, 1], outputRange: [80, 0] }), }}>
            <Image source={require('../../components/Images/Logo/logoimg.png')} style={{ width: responsiveWidth(32), height: responsiveHeight(20) }} />
          </Animated.View>

          <Animated.View style={{opacity: this.state.logoText,}}>
            <Image source={require('../../components/Images/Logo/LogoText.png')} style={{ width: responsiveWidth(35), height: responsiveHeight(7), marginTop: responsiveHeight(7) }} />
          </Animated.View>
          </View>

          <ProgressBarAndroid styleAttr="Horizontal" color="#fff"  style={{width:responsiveWidth(60),marginTop:responsiveHeight(20)}}/>
        </ImageBackground>
      </Container>
    );
  }
}

export default Splash;
