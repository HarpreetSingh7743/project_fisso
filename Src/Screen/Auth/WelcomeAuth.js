import React, { Component } from 'react';
import { View, Text, ImageBackground, Image, TouchableOpacity,StyleSheet } from 'react-native';
import { Container, Content, Icon } from 'native-base';
import { responsiveFontSize,responsiveHeight,responsiveWidth } from "react-native-responsive-dimensions";

 export default  class WelcomeAuth extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <Container>
        <ImageBackground source={require('../../components/Images/BG/SplashBG.jpg')}
        style={{height:responsiveHeight(104),width:responsiveWidth(100)}}>
          <Content>
            <View style={{height:responsiveHeight(100),width:responsiveWidth(100),
                          borderWidth:0,borderColor:'#fff',alignItems:'center',justifyContent: 'center',}}>
                  <Image source={require('../../components/Images/Logo/LogoFull.png')}
                    style={{width:responsiveWidth(62),height:responsiveHeight(12)}}/> 
                    <Text style={{color:'#fff',fontSize:responsiveFontSize(2)}}> Welcome to FISSO </Text>
                  <View style={{marginTop:responsiveWidth(30),alignItems: 'center',}}>
                    <TouchableOpacity style={styles.mainButton1}
                    onPress={()=>{this.props.navigation.navigate('SignIn')}}>
                      <Text style={styles.buttontext1}>SignIn Here</Text>
                    </TouchableOpacity>
                    <Text style={{color:'#fff',fontSize:responsiveFontSize(2.5),marginTop:responsiveWidth(10)}}>
                      --------------------or--------------------
                    </Text>
                    <View style={{flexDirection:'row',marginTop:responsiveWidth(10)}}>
                          <TouchableOpacity style={styles.socialButtonsView}>
                              <Icon name="social-facebook" type="SimpleLineIcons" style={styles.socialButtons}/>
                          </TouchableOpacity>
                          <TouchableOpacity style={styles.socialButtonsView}>
                              <Icon name="social-google" type="SimpleLineIcons" style={styles.socialButtons}/>
                          </TouchableOpacity>
                    </View>
                    <TouchableOpacity style={{marginTop:responsiveWidth(14)}}>
                      <Text style={{color:'#fff',fontWeight:'bold'}}>Continue without SignIn </Text>
                    </TouchableOpacity>
              </View>               
            </View>
          </Content>
        </ImageBackground>
      </Container>
    );
  }
}


const styles =StyleSheet.create({
  mainButton1:{
    height:responsiveHeight(8),
    width:responsiveWidth(70),
    borderColor:'#fff',
    borderWidth:2,
    borderRadius:20,
    alignItems:'center',
    justifyContent: 'center',
    backgroundColor:'rgba(1,1,1,0.4)'
  },
  buttontext1:{
    color:'#fff',
    fontWeight:'bold',
    fontSize:responsiveFontSize(2.5)
  },
  socialButtons:{
    color:'#fff',
    marginHorizontal:responsiveWidth(8)
  },
  socialButtonsView:{
    borderWidth:2,
    borderColor:'#fff',
    paddingVertical:responsiveWidth(2),
    marginHorizontal:responsiveWidth(4),
    borderRadius:30,
    alignItems:'center',
    justifyContent: 'center',
  }
})
