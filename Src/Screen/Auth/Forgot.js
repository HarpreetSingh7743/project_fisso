import React, { Component } from 'react';
import { View, ImageBackground, TextInput, KeyboardAvoidingView } from 'react-native';
import { Container, Text, Icon, Toast } from 'native-base';
import { responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions';
import styles from '../../components/Styles/Main/Styles'
import { TouchableOpacity } from 'react-native-gesture-handler';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';

class Forgot extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentScreen:"Verify",
      email:"",
      otp:'',
      password:'',
      Cpassword:''
    };
  }

  checkMail=()=>{
    var mailformat=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    var email=this.state.email

    if(email.match(mailformat)){
      this.setState({currentScreen:"otp"})
    }
    else{
      Toast.show({
        text:"Enter Email in correct format (i.e. abc@example.com)",
        textStyle:{textAlign:"center",color:'#111'},
        duration:2000,
        position:'bottom',
        style:{borderRadius:20,margin:responsiveWidth(10),backgroundColor:'rgba(255,255,255,0.8)'}
      });
    }
  }

  otpVerification=()=>{
    var {otp,password,Cpassword}=this.state
    var passformat=/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20}$/;

    if(otp=="" && password=="" && Cpassword=="")
    {
      Toast.show({
        text:"Please fill all the details",
        textStyle:{textAlign:"center",color:'#111'},
        duration:2000,
        position:'bottom',
        style:{borderRadius:20,margin:responsiveWidth(10),backgroundColor:'rgba(255,255,255,0.8)'}
      });
    }
    else{
      if(otp.length==4 && !isNaN(otp))
      {
        if(password.match(passformat))
        {
          if(password.match(passformat) && Cpassword==password)
          {
            alert("Sucess")
          }
          else{
            Toast.show({
              text:"Password didn't match",
              textStyle:{textAlign:"center",color:'#111'},
              duration:2000,
              position:'bottom',
              style:{borderRadius:20,margin:responsiveWidth(10),backgroundColor:'rgba(255,255,255,0.8)'}
            });
          }
        }
        else{
          Toast.show({
            text:"Password length must be more then 8 and a combination of Capital,Small leters and Numbers",
            textStyle:{textAlign:"center",color:'#111'},
            duration:2000,
            position:'bottom',
            style:{borderRadius:20,margin:responsiveWidth(10),backgroundColor:'rgba(255,255,255,0.8)'}
          });
        }
      }
      else{
        Toast.show({
          text:"Invalid OTP",
          textStyle:{textAlign:"center",color:'#111'},
          duration:2000,
          position:'bottom',
          style:{borderRadius:20,margin:responsiveWidth(10),backgroundColor:'rgba(255,255,255,0.8)'}
        });
      }
    }
  }
  renderScreen(){
    if(this.state.currentScreen == "Verify")
    {
      return(
        <View style={{alignItems:'center'}}>
          <View style={{flexDirection:'row', margin:responsiveWidth(3),
              borderRadius:20, borderWidth:2, borderColor:'#fff',
              padding:responsiveWidth(2), alignItems:'center', width:responsiveWidth(80)}}>
              <Icon name="mail" type="AntDesign" style={{color:'#fff'}}/>
              <TextInput allowFontScaling={false} 
                style={{width:responsiveWidth(65), borderBottomWidth:1, borderColor:'#fff',
                marginLeft:responsiveWidth(1), color:'#fff'}} 
                placeholder="Enter Email" placeholderTextColor="#fff"
                onChangeText={(email)=>{this.setState({email:email})}}
                keyboardType="email-address"
                autoCorrect={false}
                returnKeyType="go"/>
          </View>
          <View style={{marginTop:responsiveWidth(5)}}>
          <TouchableOpacity style={styles.mainButtons}
            onPress={()=>{this.checkMail()}}>
            <Text style={styles.buttonText}>Get OTP</Text>
          </TouchableOpacity>
          </View>
        </View>
      )
    }
    else if(this.state.currentScreen=="otp")
    {
      return(
        <View style={{alignItems:'center'}}>
          <View style={{flexDirection:'row', margin:responsiveWidth(3),
              borderRadius:20, borderWidth:2, borderColor:'#fff',
              padding:responsiveWidth(2), alignItems:'center', width:responsiveWidth(80)}}>
            <Icon name="pencil" type="SimpleLineIcons" style={{color:'#fff'}}/>
            <TextInput style={{width:responsiveWidth(65), borderBottomWidth:1, borderColor:'#fff',
              marginLeft:responsiveWidth(1), color:'#fff'}} 
              placeholder="Enter OTP" 
              placeholderTextColor="#fff"
              onChangeText={(otp)=>{this.setState({otp:otp})}}
              allowFontScaling={false}
              keyboardType="number-pad"
              returnKeyType="next"
              onSubmitEditing={()=>{this.refs.pass.focus()}}/>

          </View>
          <View style={{flexDirection:'row', margin:responsiveWidth(3),
              borderRadius:20, borderWidth:2, borderColor:'#fff',
              padding:responsiveWidth(2), alignItems:'center' , width:responsiveWidth(80)}}>
            <Icon name="unlock" type="Feather" style={{color:'#fff'}}/>
            <TextInput style={{width:responsiveWidth(65), borderBottomWidth:1, borderColor:'#fff',
              marginLeft:responsiveWidth(1), color:'#fff'}} 
              placeholder="Enter New Password" 
              placeholderTextColor="#fff" 
              secureTextEntry={true}
              onChangeText={(password)=>{this.setState({password:password})}}
              allowFontScaling={false}
              keyboardType="default"
              returnKeyType="next"
              ref={'pass'}
              onSubmitEditing={()=>{this.refs.cpass.focus()}}/>
          </View>
          <View style={{flexDirection:'row', margin:responsiveWidth(3),
              borderRadius:20, borderWidth:2, borderColor:'#fff',
              padding:responsiveWidth(2),alignItems:'center', width:responsiveWidth(80)}}>
            <Icon name="unlock" type="Feather" style={{color:'#fff'}}/>
            <TextInput style={{width:responsiveWidth(65), borderBottomWidth:1, borderColor:'#fff',
              marginLeft:responsiveWidth(1), color:'#fff'}} 
              placeholder="Confirm New Password" 
              placeholderTextColor="#fff" 
              secureTextEntry={true}
              onChangeText={(Cpassword)=>{this.setState({Cpassword:Cpassword})}}
              allowFontScaling={false}
              keyboardType="default"
              returnKeyType="go"
              ref={'cpass'}
              onSubmitEditing={()=>{this.otpVerification()}}/>
          </View>
          <View style={{flexDirection:'row',marginTop:responsiveWidth(4)}}>
            <View style={{flex:0.3}}></View>
            <View style={{flex:0.7,alignItems:'flex-end'}}>
              <TouchableOpacity onPress={()=>{this.setState({currentScreen:'Verify'})}}>
                <Text style={{color:'#fff', fontWeight:'bold'}} allowFontScaling={false}>Change Email ?</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={{marginTop:responsiveWidth(5)}}>
            <TouchableOpacity style={styles.mainButtons} 
              onPress={()=>{this.otpVerification()}}>
              <Text style={styles.buttonText}>Reset Password</Text>
            </TouchableOpacity>
          </View>
        </View>
      )
    }
  }
  render() {
    return (
      <Container style={{flex : 1, justifyContent : 'center', alignItems : 'center'}}>
          <ImageBackground source={require('../../../Src/components/Images/BG/Auth.jpg')}
            style={{width : responsiveWidth(100), height : responsiveHeight(104)}} resizeMethod="scale">
            <KeyboardAvoidingView  behavior={Platform.OS === "ios" ? "position" : 'padding'}>    
              <View style={{width : wp("100%"), height : hp('65%'), 
                borderWidth : 0, borderColor:'#f11', marginTop : hp("30%")}}>
                <View style={{flex:1, margin:responsiveWidth(4), 
                  backgroundColor:'rgba(1,1,1,0.8)', borderRadius:20, padding:responsiveWidth(2)}}>
                  <Text style={styles.authHeadText}>Forgot Password</Text>
                  <View style={{width:responsiveWidth(88), alignItems: 'center',
                      justifyContent:'center', borderWidth:0, borderColor:'#fff'}}>
                    {this.renderScreen()}
                  </View>
                </View>
              </View>      
            </KeyboardAvoidingView>  
          </ImageBackground>
      </Container>
    );
  }
}

export default Forgot;
