import React, { Component } from 'react';
import { View, ImageBackground, TextInput, KeyboardAvoidingView, Platform } from 'react-native';
import { Container, Content, Text, Icon, CheckBox, Card,Toast } from 'native-base';
import { responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import styles from '../../components/Styles/Main/Styles'
import { TouchableOpacity } from 'react-native-gesture-handler';

class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      check:false,
      Fname:'',
      Email:'',
      password:'',
      Cpassword:'',
      field1:'#fff',
      field2:'#fff',
      field3:'#fff',
      field4:'#fff'
    };
  }

  verifyDetails=()=>{
    var {Fname,Email,password,Cpassword}=this.state
    var mailformat=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    var passformat=/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20}$/;

    if(Fname=="" || Email==""|| password==""|| Cpassword=="")
    {
      Toast.show({
        text:"Please Enter all Fields",
        textStyle:{textAlign:"center",color:'#111'},
        duration:2000,
        position:'bottom',
        style:{borderRadius:20,margin:responsiveWidth(10),backgroundColor:'rgba(255,255,255,0.8)'}
      });
    }
    else{
      if(Fname.length>3){
        this.setState({field1:'#1f1'})
        if(Email.match(mailformat))
        {
          this.setState({field2:'#1f1'})
          if(password.match(passformat))
          {
            this.setState({field3:'#1f1'})
            if(password.match(passformat) && Cpassword===password)
            {this.setState({field4:'#1f1'})
              if(this.state.check==true)
              {
                alert('Sucess')
              }
              else{
                Toast.show({
                  text:"Tick the checkbox for confirmation",
                  textStyle:{textAlign:"center",color:'#111'},
                  duration:2000,
                  position:'bottom',
                  style:{borderRadius:20,margin:responsiveWidth(10),backgroundColor:'rgba(255,255,255,0.8)'}
                });
              }
             }
            else{
              this.setState({field4:'#fff'})
              Toast.show({
                text:"Password didn't match",
                textStyle:{textAlign:"center",color:'#111'},
                duration:2000,
                position:'bottom',
                style:{borderRadius:20,margin:responsiveWidth(10),backgroundColor:'rgba(255,255,255,0.8)'}
              });
            }
          }
          else{
            this.setState({field3:'#fff'})
            Toast.show({
              text:"Password length must be more then 8 and a combination of Capital,Small leters and Numbers",
              textStyle:{textAlign:"center",color:'#111'},
              duration:4000,
              position:'bottom',
              style:{borderRadius:20,margin:responsiveWidth(10),backgroundColor:'rgba(255,255,255,0.8)'}
            });
          }
        }
        else{
          this.setState({field2:'#fff'})
          Toast.show({
            text:"Enter Email in correct format (i.e. abc@example.com)",
            textStyle:{textAlign:"center",color:'#111'},
            duration:2000,
            position:'bottom',
            style:{borderRadius:20,margin:responsiveWidth(10),backgroundColor:'rgba(255,255,255,0.8)'}
          });
        }
      }
      else{
        this.setState({field1:'#fff'})
        Toast.show({
          text:"Please Enter full Name",
          textStyle:{textAlign:"center",color:'#111'},
          duration:2000,
          position:'bottom',
          style:{borderRadius:20,margin:responsiveWidth(10),backgroundColor:'rgba(255,255,255,0.8)'}
        });
      }
    }
  }

  render() {
    return (
      <Container style={{flex:1,justifyContent:'center',alignItems:'center'}}>
        <ImageBackground source={require('../../../Src/components/Images/BG/Auth.jpg')}
          style={{width:responsiveWidth(100),height:responsiveHeight(104)}} resizeMethod="scale">
          <KeyboardAvoidingView behavior={Platform.OS === 'ios'?'position':'padding'} keyboardVerticalOffset={-64}>
            <Card style={{backgroundColor:'rgba(1,1,1,0)', borderColor:'#111', marginTop:hp('25%'),
                width : wp("90%"), height : hp((Platform.OS === 'ios')?"70%":"75%"), 
                backgroundColor:'rgba(1,1,1,0.8)', padding:responsiveWidth(2), 
                borderRadius:20, alignSelf:'center'}}>
              <Text style={styles.authHeadText} allowFontScaling={false}>SignUp</Text>
              <View style={{width:responsiveWidth(88), alignItems: 'center',
                justifyContent: 'center', borderWidth:0, borderColor:'#fff',
                marginTop:responsiveWidth(3)}}>
                <View style={{flexDirection:'row', margin:responsiveWidth(3),
                  borderRadius:20, borderWidth:2, borderColor:this.state.field1,
                  padding:responsiveWidth(2), alignItems:'center', width:responsiveWidth(80)}}>
                  <Icon name="user" type="SimpleLineIcons" style={{color:this.state.field1}}/>
                  <TextInput style={{width:responsiveWidth(65), borderBottomWidth:1,
                    borderColor:'#fff', marginLeft:responsiveWidth(1),color:"#fff"}} 
                    placeholder="Enter Full Name" 
                    placeholderTextColor="#fff"
                    onChangeText={(Fname)=>this.setState({Fname:Fname})}
                    allowFontScaling={false}
                    autoCorrect={false}
                    keyboardType="email-address"
                    returnKeyType="next"
                    onSubmitEditing={()=>{this.refs.email.focus()}}/>
                </View>
                <View style={{flexDirection:'row', margin:responsiveWidth(3),
                  borderRadius:20, borderWidth:2, borderColor:this.state.field2,
                  padding:responsiveWidth(2), alignItems:'center', width:responsiveWidth(80)}}>
                  <Icon name="mail" type="AntDesign" style={{color:this.state.field2}}/>
                  <TextInput style={{width:responsiveWidth(65), borderBottomWidth:1,
                    borderColor:'#fff', marginLeft:responsiveWidth(1), color:'#fff'}} 
                    placeholder="Enter Email" 
                    placeholderTextColor="#fff"
                    onChangeText={(Email)=>this.setState({Email:Email})}
                    allowFontScaling={false}
                    autoCorrect={false}
                    keyboardType="default"
                    returnKeyType="next"
                    ref={'email'}
                    onSubmitEditing={()=>{this.refs.pass.focus()}}/>
                </View>
                <View style={{flexDirection:'row', margin:responsiveWidth(3),
                    borderRadius:20, borderWidth:2, borderColor:this.state.field3,
                    padding:responsiveWidth(2), alignItems:'center', width:responsiveWidth(80)}}>
                  <Icon name="unlock" type="Feather" style={{color:this.state.field3}}/>
                  <TextInput style={{width:responsiveWidth(65), borderBottomWidth:1,
                    borderColor:'#fff', marginLeft:responsiveWidth(1), color:"#fff"}} 
                    placeholder="Enter Password" 
                    placeholderTextColor="#fff" 
                    secureTextEntry={true}
                    onChangeText={(password)=>this.setState({password:password})}
                    allowFontScaling={false}
                    autoCorrect={false}
                    keyboardType="default"
                    returnKeyType="next"
                    ref={'pass'}
                    onSubmitEditing={()=>{this.refs.cpass.focus()}}/>
                </View>
                <View style={{flexDirection:'row', margin:responsiveWidth(3),
                  borderRadius:20, borderWidth:2, borderColor:this.state.field4,
                  padding:responsiveWidth(2), alignItems:'center', width:responsiveWidth(80)}}>
                  <Icon name="unlock" type="Feather" style={{color:this.state.field4}}/>
                  <TextInput style={{width:responsiveWidth(65), borderBottomWidth:1,
                    borderColor:'#fff', marginLeft:responsiveWidth(1), color:'#fff'}} 
                    placeholder="Confirm Password" 
                    placeholderTextColor="#fff" 
                    secureTextEntry={true}
                    onChangeText={(Cpassword)=>this.setState({Cpassword:Cpassword})}
                    allowFontScaling={false}
                    autoCorrect={false}
                    keyboardType="default"
                    returnKeyType="go"
                    ref={'cpass'}
                    onSubmitEditing={()=>{this.verifyDetails()}}/>
                </View>
              </View>
              <View style={{width:responsiveWidth(88), marginTop:responsiveWidth(4)}}>
                <TouchableOpacity style={{flexDirection:'row', alignItems: 'center'}}
                  onPress={()=>{this.setState({check:!this.state.check})}}>
                  <CheckBox style={{marginRight:responsiveWidth(4)}} checked={this.state.check}/>
                  <Text style={styles.normalText} allowFontScaling={false}>I hereby confirm to be 18 years old</Text>
                </TouchableOpacity>
              </View>
              <View style={{width:responsiveWidth(88), flexDirection:'row',
                  marginTop:responsiveWidth(14), alignItems:'center', justifyContent:'center'}}>
                <TouchableOpacity style={styles.mainButtons} onPress={()=>{this.props.navigation.navigate('SignIn')}}>
                  <Text style={styles.buttonText} allowFontScaling={false}>SignIn</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.mainButtons} onPress={()=>{this.verifyDetails()}}>
                  <Text style={styles.buttonText} allowFontScaling={false}>SignUp</Text>
                </TouchableOpacity>
              </View>     
            </Card>
          </KeyboardAvoidingView>
        </ImageBackground>
      </Container>
    );
  }
}

export default SignUp;
