import React, { Component } from 'react';
import { View,ImageBackground, TextInput, KeyboardAvoidingView } from 'react-native';
import { Container ,Text, Icon, Card,Toast } from 'native-base';
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions';
import styles from '../../components/Styles/Main/Styles'
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { TouchableOpacity } from 'react-native-gesture-handler';

class SignIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email:'',
      pass:'',
      otp:''
    };
  }

  verifyDetails(){
    var {email,pass}=this.state
    var mailformat=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    var passformat=/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20}$/;

    if(email=="" || pass=="")
    {
      Toast.show({
        text:"Please fill all the details",
        textStyle:{textAlign:"center",color:'#111'},
        duration:2000,
        position:'bottom',
        style:{borderRadius:20,margin:responsiveWidth(10),backgroundColor:'rgba(255,255,255,0.8)'}
      });
    }
    else{
      if(email.match(mailformat))
      {
        if(pass.match(passformat))
        {
          alert('Sucess')
        }
        else{
          this.setState({field3:'#fff'})
            Toast.show({
              text:"Wrong Combination of Email and password",
              textStyle:{textAlign:"center",color:'#111'},
              duration:4000,
              position:'bottom',
              style:{borderRadius:20,margin:responsiveWidth(10),backgroundColor:'rgba(255,255,255,0.8)'}
            });
        }
      }
      else{
        Toast.show({
          text:"Enter Email in correct format (i.e. abc@example.com)",
          textStyle:{textAlign:"center",color:'#111'},
          duration:2000,
          position:'bottom',
          style:{borderRadius:20,margin:responsiveWidth(10),backgroundColor:'rgba(255,255,255,0.8)'}
        });
      }
    }
  }
  render() {

    return (
      <Container style={{flex:1,justifyContent: 'center',alignItems: 'center',}}>
        <ImageBackground source={require('../../../Src/components/Images/BG/Auth.jpg')}
          style={{width:responsiveWidth(100),height:responsiveHeight(104)}} resizeMethod="scale">
          <KeyboardAvoidingView behavior={Platform.OS === 'ios'?'position':'padding'} keyboardVerticalOffset={-64}>
            <Card style={{backgroundColor:'rgba(1,1,1,0)', borderColor:'#111', marginTop:hp('35%'),
                width : wp("90%"), height : hp((Platform.OS === 'ios')?"50%":"55%"), 
                backgroundColor:'rgba(1,1,1,0.8)', padding:responsiveWidth(2), 
                borderRadius:20, alignSelf:'center'}}>
              <Text style={styles.authHeadText} allowFontScaling={false}>Login</Text>
              <View style={{width:responsiveWidth(88), alignItems: 'center',
                  justifyContent: 'center', borderWidth:0, borderColor:'#fff'}}>
                <View style={{flexDirection:'row', margin:responsiveWidth(3),
                    borderRadius:20,borderWidth:2,borderColor:'#fff',
                    padding:responsiveWidth(2),alignItems:'center', width:responsiveWidth(80)}}>
                  <Icon name="mail" type="AntDesign" style={{color:'#fff'}}/>
                  <TextInput style={{width:responsiveWidth(65), borderBottomWidth:1, borderColor:'#fff',marginLeft:responsiveWidth(1),color:'#fff'}} 
                    onChangeText={(email)=>{this.setState({email:email})}}
                    placeholder="Enter Email"
                    placeholderTextColor="#fff"
                    allowFontScaling={false}
                    keyboardType="email-address"
                    returnKeyType="next"
                    onSubmitEditing={()=>{this.refs.pass.focus()}}/>
                </View>
                <View style={{flexDirection:'row', margin:responsiveWidth(3),
                    borderRadius:20, borderWidth:2, borderColor:'#fff',
                    padding:responsiveWidth(2), alignItems:'center', width:responsiveWidth(80)}}>
                  <Icon name="unlock" type="Feather" style={{color:'#fff'}}/>
                  <TextInput style={{width:responsiveWidth(65), borderBottomWidth:1, borderColor:'#fff',marginLeft:responsiveWidth(1),color:'#fff'}} 
                      onChangeText={(pass)=>{this.setState({pass:pass})}}
                      placeholder="Enter Password" 
                      placeholderTextColor="#fff" 
                      secureTextEntry={true}
                      allowFontScaling={false}
                      keyboardType="default"
                      ref={'pass'}
                      returnKeyType="go"
                      autoCorrect={false}/>
                </View>               
              </View>
              <View style={{width:responsiveWidth(88),flexDirection:'row',marginTop:responsiveWidth(8)}}>
                <View style={{flex:0.4}}></View>
                <View style={{flex:0.6,alignItems:'flex-end'}}>
                    <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Forgot')}}>
                      <Text style={{color:'#fff',fontWeight:'bold'}}
                        allowFontScaling={false}>Forgot Password ?</Text>
                    </TouchableOpacity>
                </View>
              </View>
              <View style={{width:responsiveWidth(88), flexDirection:'row',
                  marginTop:responsiveWidth(4), alignItems:'center',
                  justifyContent:'center'}}>
                <TouchableOpacity style={styles.mainButtons} onPress={()=>{this.props.navigation.navigate('SignUp')}}>
                  <Text style={styles.buttonText} allowFontScaling={false}>SignUp</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.mainButtons}
                onPress={()=>{this.verifyDetails()}}>
                  <Text style={styles.buttonText} allowFontScaling={false}>SignIn</Text>
                </TouchableOpacity>
              </View>
              <View style={{width:responsiveWidth(88), alignItems:'center', 
                justifyContent: 'center', marginTop:responsiveWidth(13)}}>
                
              </View>      
            </Card>
          </KeyboardAvoidingView>
        </ImageBackground>
      </Container>
    );
  }
}

export default SignIn;
