import React, { Component } from 'react';
import { View, Text, ImageBackground, SafeAreaView, TouchableOpacity, Modal, Image } from 'react-native';
import { Container, Content, Card } from 'native-base';
import { responsiveFontSize, responsiveHeight, responsiveWidth } from "react-native-responsive-dimensions";
import Styles from '../../components/Styles/Main/Styles';
import { Avatar } from 'react-native-elements';

import UserAvatar from '../../components/Styles/CustomComponents/UserAvatar'

import ChangePasswordModal from '../../components/Styles/CustomComponents/Modals/ChangePasswordModal'
import UpdateProfileModal from '../../components/Styles/CustomComponents/Modals/UpdateProfileModal'

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: "Harpreet Singh",
      userEmail: "Hsingh7743@gmail.com",
      userMobile: "9501205790",
      servicesBooked: 4,
      userAddress: "#2041, Sector 41 C, Chandigarh",
      VisiblePasswordModal: false,
      VisibleprofileModal: false
    };
  }

  render() {
    return (
      <Container>
        <ImageBackground
          source={require('../../components/Images/BG/Back.jpg')}
          style={{ width: responsiveWidth(100), height: responsiveHeight(104) }} resizeMode="cover">
          <SafeAreaView>
            {/* ChangePasswordModal here */}
            {this.state.VisiblePasswordModal && <ChangePasswordModal onExit={(val) => { this.setState({ VisiblePasswordModal: val }) }} />}

            {/* UpdateProfileModal here  */}
            {this.state.VisibleprofileModal && <UpdateProfileModal onExit={(val) => { this.setState({ VisibleprofileModal: val }) }} />}

            <View style={{ width: responsiveWidth(100), height: responsiveHeight(10), padding: responsiveWidth(2), justifyContent: 'flex-end' }}>
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Text style={Styles.TitleText}
                  allowFontScaling={false}>Profile</Text>
              </View>
            </View>
            <View style={{ width: responsiveWidth(100), height: responsiveHeight(89) }}>
              <View style={{ width: responsiveWidth(100), height: responsiveHeight(25), flexDirection: 'row' }}>
                <View style={{ flex: 0.63, padding: responsiveWidth(2), justifyContent: 'center', }}>
                  <Text style={Styles.headerText}
                    allowFontScaling={false}>{this.state.userName}</Text>
                  <Text style={Styles.boldText}
                    allowFontScaling={false}>{this.state.userEmail}</Text>
                </View>
                <View style={{ flex: 0.37, padding: responsiveWidth(2), alignItems: 'flex-end', justifyContent: 'center' }}>
                  <UserAvatar />
                </View>
              </View>
              <View style={{ paddingHorizontal: responsiveWidth(2) }}>
                <Text style={Styles.IntermediateText}
                  allowFontScaling={false}>Personal Information</Text>
              </View>
              <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                <View style={{ width: responsiveWidth(90), height: responsiveHeight(55), borderRadius: 15, borderColor: '#444', backgroundColor: 'rgba(1,1,1,0.1)' }}>
                  <View style={{ flex: 0.5, padding: responsiveWidth(6) }}>
                    <View style={{ flexDirection: 'row', marginVertical: responsiveWidth(3) }}>
                      <Text style={Styles.boldText}
                        allowFontScaling={false}>Mobile :  </Text>
                      <Text style={Styles.normalText}
                        allowFontScaling={false}>{this.state.userMobile}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', marginVertical: responsiveWidth(3) }}>
                      <Text style={Styles.boldText}
                        allowFontScaling={false}>Services Booked :  </Text>
                      <Text style={Styles.normalText}
                        allowFontScaling={false}>{this.state.servicesBooked}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', marginVertical: responsiveWidth(3) }}>
                      <Text style={Styles.boldText}
                        allowFontScaling={false}>Password :  </Text>
                      <Text style={Styles.normalText}
                        allowFontScaling={false}>*********</Text>
                      <TouchableOpacity onPress={() => { this.setState({ VisiblePasswordModal: true }) }}>
                        <Text style={{ color: '#2B6DFF', marginLeft: responsiveWidth(25) }}
                          allowFontScaling={false}>Change</Text>
                      </TouchableOpacity>
                    </View>
                    <View style={{ flexDirection: 'row', marginVertical: responsiveWidth(3) }}>
                      <Text style={Styles.boldText}
                        allowFontScaling={false}>Address :  </Text>
                      <Text style={Styles.normalText}
                        allowFontScaling={false}>{this.state.userAddress}</Text>
                    </View>



                  </View>
                  <View style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: '' }}>
                    <TouchableOpacity style={Styles.mainButtons} onPress={() => { this.setState({ VisibleprofileModal: true }) }}>
                      <Text style={Styles.buttonText}
                        allowFontScaling={false}>Update Profile</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </View>
          </SafeAreaView>
        </ImageBackground>
      </Container>
    );
  }
}

export default Profile;
