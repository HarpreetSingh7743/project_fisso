import React, { Component } from 'react';
import { View, Text, ImageBackground, ScrollView, TouchableOpacity, Platform, Alert } from 'react-native';
import { Container, Card, CardItem } from 'native-base';
import { responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions';
import Styles from '../../components/Styles/Main/Styles'
import { SafeAreaView } from 'react-native-safe-area-context';
import ViewRequestedServices from '../../components/Styles/CustomComponents/Modals/ViewRequestedServices';
class Requests extends Component {
    constructor(props) {
        super(props);
        this.state = {
            visibleRequestedServicesModal: false,
            requestDetails: [
                {
                    key: 1,
                    VehicleName: "Alto 800",
                    VehcileYear: 2016,
                    GarageName: "ABC Garage",
                    GarageEmail: "hello@gmail.com",
                    GarageAddress: "ABC Nagar ,Mohali",
                    servicesDetails: [
                        { ServiceName: "Wheel Alignment", Price: 0 },
                        { ServiceName: "COVID 19 Sanitaziation", Price: 0 },
                        { ServiceName: "Coolant Replacement:", Price: 0 },
                        { ServiceName: "Brakeoil Replacement", Price: 0 },
                        { ServiceName: "Engine Checkup", Price: 0 },
                        { ServiceName: "Insuarance", Price: 0 }
                    ]
                },
                {
                    key: 2,
                    VehicleName: "Maruti Swift",
                    VehcileYear: 2017,
                    GarageName: "PQR Garage",
                    GarageEmail: "pqrgarage@gmail.com",
                    GarageAddress: "pqr Nagar ,Mohali",
                    servicesDetails: [
                        { ServiceName: "Wheel Balancing", Price: 500 },
                        { ServiceName: "COVID 19 Sanitaziation", Price: 2000 },
                        { ServiceName: "Brakeoil Replacement:", Price: 2500 }
                    ]
                },
                {
                    key: 3,
                    VehicleName: "Honda City",
                    VehcileYear: 2019,
                    GarageName: "HondaOfficial Garage",
                    GarageEmail: "newhona@gmail.com",
                    GarageAddress: "SAS Nagar ,Mohali",
                    servicesDetails: [
                        { ServiceName: "Wheel Alignment", Price: 500 },
                        { ServiceName: "Engine Checkup", Price: 2000 },
                        { ServiceName: "Coolant Replacement:", Price: 2500 },
                    ]
                },
            ],
            Temp: [],
            TotalBill: 0
        };
    }


    handleClick(t) {
        let Arr=this.state.Temp
        let total=0
        for(let i=0;i<Arr.length;i++){
           total=total+ Arr[i].Price;
        }
        if (total != 0) {
            this.props.navigation.navigate("Appointments", {
                VehicleName: t.VehicleName,
                VehcileYear: t.VehcileYear,
                GarageName: t.GarageName,
                GarageEmail: t.GarageEmail,
                GarageAddress: t.GarageAddress,
                servicesDetails: this.state.Temp
            })
        }
        else{
            alert("You haven't recieved the Bill from the garage for your requested services. Please wait for the garage to send you the total bill before selecting Appointment time.")
        }
    }
    renderItems() {
        return this.state.requestDetails.map((t) => {
            return (
                <Card key={t.key} style={{ width: responsiveWidth(95), height: responsiveHeight(23), borderColor: '#111', backgroundColor: 'rgba(255,255,255,0.15)', marginTop: responsiveWidth(2) }}>


                    {this.state.visibleRequestedServicesModal && <ViewRequestedServices servicesDetails={this.state.Temp} onExit={(val) => { this.setState({ visibleRequestedServicesModal: val }) }} />}


                    <CardItem style={{ backgroundColor: 'rgba(255,255,255,0.7)' }}>
                        <Text style={{ fontWeight: 'bold', color: '#111' }}>{t.VehicleName} {t.VehcileYear}</Text>
                    </CardItem>
                    <View style={{ flex: 0.65, flexDirection: 'row' }}>
                        <View style={{ flex: 0.7, justifyContent: 'center', marginLeft: responsiveWidth(2) }}>
                            <Text style={Styles.normalText}>Garage : {t.GarageName}</Text>
                            <Text style={Styles.normalText}>Email: {t.GarageEmail}</Text>
                            <Text style={Styles.normalText}>Address: {t.GarageAddress}</Text>
                        </View>
                        <View style={{ flex: 0.3, alignItems: 'center', justifyContent: 'center' }}>
                            <TouchableOpacity onPress={() => { this.setState({ Temp: t.servicesDetails, visibleRequestedServicesModal: true }) }}>
                                <Text style={{ color: '#33f' }}>View Bill</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{ flex: 0.35, flexDirection: 'row', alignItems: 'center', justifyContent: 'center', }}>
                        <TouchableOpacity style={{
                            width: responsiveWidth(40),
                            height: responsiveHeight(5),
                            borderColor: '#fff',
                            borderWidth: 1,
                            marginHorizontal: responsiveWidth(5),
                            justifyContent: 'center',
                            alignItems: 'center',
                            borderRadius: 10
                        }}
                            onPress={() => { this.cancelAppointment(t) }}>
                            <Text style={{ color: '#fff', fontWeight: 'bold' }}>Cancel</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={{
                            width: responsiveWidth(40),
                            height: responsiveHeight(5),
                            borderColor: '#fff',
                            borderWidth: 1,
                            marginHorizontal: responsiveWidth(5),
                            justifyContent: 'center',
                            alignItems: 'center',
                            borderRadius: 10
                        }}
                            onPressIn={() => { this.setState({ Temp: t.servicesDetails }) }}
                            onPressOut={() => { this.handleClick(t) }}>
                            <Text style={{ color: '#fff', fontWeight: 'bold' }}>Accept Deal</Text>
                        </TouchableOpacity>
                    </View>
                </Card>
            )
        })
    }
    cancelAppointment(t) {
        let arr = this.state.requestDetails
        let pos = arr.indexOf(t)
        let VehicleName = t.VehicleName
        Alert.alert(
            "Cancel Appointment ?",
            "Are you sure you want to cancel " + VehicleName + "'s Appointment?",
            [
                {
                    text: "Cancel",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                },
                {
                    text: "YES", onPress: () => {
                        arr.splice(pos, 1)
                        this.setState({ requestDetails: arr })
                    }
                }
            ],
            { cancelable: false }
        );

    }
    render() {
        return (
            <Container>
                <ImageBackground source={require('../../components/Images/BG/Back.jpg')}
                    style={{ width: responsiveWidth(100), height: responsiveHeight(104) }}>

                    <View style={{ width: responsiveWidth(100), height: responsiveHeight(8), justifyContent: Platform.OS === "android" ? 'flex-end' : "center" }}>
                        <Text style={Styles.TitleText}
                            allowFontScaling={false}>Requests</Text>
                    </View>
                    <View style={{ width: responsiveWidth(100), height: responsiveHeight(85), alignItems: 'center' }}>
                        <ScrollView showsVerticalScrollIndicator={false}>
                            {this.renderItems()}
                        </ScrollView>
                    </View>
                </ImageBackground>
            </Container>
        );
    }
}

export default Requests;
