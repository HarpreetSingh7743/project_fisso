import React, { Component } from 'react';
import { Text, ImageBackground, TouchableOpacity, SafeAreaView } from 'react-native';
import { Container, Icon } from 'native-base';
import styles from '../../components/Styles/Main/Styles';

var selected = null;

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
        UserName:'Harpreet Singh',
        UserEmail:'Hsingh7743@gmail.com',
        type: null,
    };
  }

  onclickbike = () =>{
      this.setState({ "type" : "bike"})
  }

  onclickcar = () =>{
    this.setState({ "type" : "car" }) 
  }

  render() {
    const { navigate } = this.props.navigation
    selected = this.state.type

    return (
      <Container>
          <ImageBackground
            source={require('../../components/Images/BG/Home.jpg')} style={{flex:1}}>
                
            <SafeAreaView style={{flex:0.15,borderWidth:0,borderColor:'#fff',flexDirection:'row',alignItems:'flex-end'}}>
                <SafeAreaView style={{flex:0.6,borderWidth:0,borderColor:'#fff'}}>
                    <Text style={styles.headerText}
                        allowFontScaling={false}>{this.state.UserName}</Text>
                    <Text style={styles.normalText}
                        allowFontScaling={false}>{this.state.UserEmail}</Text>
                </SafeAreaView>
                <SafeAreaView style={{flex:0.4,borderWidth:0,borderColor:'#fff',flexDirection:'row',justifyContent:'flex-end'}}>
                    <TouchableOpacity style={styles.roundedButtons} onPress={()=>{this.props.navigation.navigate("Notification")}}>
                        <Icon name="bell-o" type="FontAwesome" style={{color:'#fff'}}/>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.roundedButtons}  onPress={()=>{this.props.navigation.navigate("Profile")}}>
                        <Icon name="user" type="Feather" style={{color:'#fff'}}/>
                    </TouchableOpacity>
                </SafeAreaView>
            </SafeAreaView>
            <SafeAreaView style={{flex:0.7,borderWidth:0,borderColor:'#fff',justifyContent:'flex-end'}}>
                <Text style={styles.headerText}
                    allowFontScaling={false}>Select Vehicle</Text>
            </SafeAreaView>
            <SafeAreaView style={{flex:0.15,borderWidth:0,borderColor:'#fff',flexDirection:'row',alignItems:'center',justifyContent:'center'}}>
                <TouchableOpacity style={styles.mainButtons}  onPressIn={()=>this.onclickbike()}
                  onPressOut={()=>this.props.navigation.navigate('SelectVehicle', {selected:selected})}>
                  <Text style={styles.buttonText}
                    allowFontScaling={false}>Bike</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.mainButtons} onPressIn={()=>this.onclickcar()}
                  onPressOut={()=>this.props.navigation.navigate('SelectVehicle', {selected:selected})}>
                  <Text style={styles.buttonText}
                    allowFontScaling={false}>Car</Text>
                </TouchableOpacity>
            </SafeAreaView>

          </ImageBackground>
      </Container>
    );
  }
}

export default Dashboard;
