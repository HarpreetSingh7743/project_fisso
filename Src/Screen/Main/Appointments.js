import React, { Component } from 'react';
import { View, Text, ImageBackground, SafeAreaView, TextInput, Image } from 'react-native';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Container, Card, Icon, Toast } from 'native-base';
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import Styles from '../../components/Styles/Main/Styles';
import moment from 'moment'

export default class Appointments extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisible: false,
      chosenDate: 'Select Appointment Date',
      chosenTime: "",
      bar1: '#111',
      bar2: '#111',
      bar3: '#111',
      bar4: '#111',
      VehicleName: this.props.navigation.state.params.VehicleName,
      VehcileYear: this.props.navigation.state.params.VehcileYear,
      GarageName: this.props.navigation.state.params.GarageName,
      GarageEmail: this.props.navigation.state.params.GarageEmail,
      GarageAddress: this.props.navigation.state.params.GarageAddress,
      servicesDetails: this.props.navigation.state.params.servicesDetails
    };
  }
  handlePicker = (date) => {
    this.setState({ isVisible: false, chosenDate: moment(date).format('MM/DD/YYYY') })
  }

  showPicker = () => {
    this.setState({ isVisible: true })
  }
  hidePicker = () => {
    this.setState({ isVisible: false })
  }

  verifyDetails() {
    var { chosenDate, chosenTime, bar1, bar2, bar3, bar4 } = this.state
    var Day = new Date().getDate()
    var Month = new Date().getMonth() + 1
    var Year = new Date().getFullYear()
    var Hours = new Date().getHours()
    var Minutes = new Date().getMinutes()
    var Seconds = new Date().getMinutes()

    var currentDateTime = Month + "/" + Day + "/" + Year + " " + Hours + ":" + Minutes + ":" + Seconds
    var chosenDateTime = chosenDate + " " + chosenTime

    if (chosenDate == "Select Appointment Date" || bar1 == '#111' && bar2 == '#111' && bar3 == '#111' && bar4 == '#111') {
      Toast.show({
        text: "Please fill required Fields",
        textStyle: { textAlign: "center", color: '#111' },
        duration: 4000,
        position: 'bottom',
        style: { borderRadius: 20, margin: responsiveWidth(10), backgroundColor: 'rgba(255,255,255,0.8)' }
      });
    }
    else {
      var datum = Date.parse(chosenDateTime);
      var Chosenresult = datum / 1000;

      var datum1 = Date.parse(currentDateTime);
      var CurrentResult = datum1 / 1000;

      var diffrence = Chosenresult - CurrentResult

      if (CurrentResult < Chosenresult) {
        if (diffrence <= 345600)
        // 345600 is timestamp of 4 days
        {
          alert("Booked")
          this.props.navigation.navigate("MainAppNav")
        }
        else {
          Toast.show({
            text: "Appointment can be booked for next 4 days only",
            textStyle: { textAlign: "center", color: '#111' },
            duration: 4000,
            position: 'bottom',
            style: { borderRadius: 20, margin: responsiveWidth(10), backgroundColor: 'rgba(255,255,255,0.8)' }
          })
        }
      }
      else {
        Toast.show({
          text: "Can't Book in Past",
          textStyle: { textAlign: "center", color: '#111' },
          duration: 4000,
          position: 'bottom',
          style: { borderRadius: 20, margin: responsiveWidth(10), backgroundColor: 'rgba(255,255,255,0.8)' }
        })
      }
    }
  }

  render() {
    return (
      <Container>
        <ImageBackground source={require('../../components/Images/BG/Back.jpg')}
          style={{ height: responsiveHeight(104), width: responsiveWidth(100) }} resizeMode="cover">
          <SafeAreaView style={{ width: responsiveWidth(100), height: responsiveHeight(15), justifyContent: 'flex-end' }}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text style={Styles.TitleText} allowFontScaling={false}>Appointment</Text>
            </View>
          </SafeAreaView>
          <SafeAreaView style={{ alignItems: 'center', height: responsiveHeight(89) }}>
            <Card style={{
              height: responsiveHeight(60),
              width: responsiveWidth(95),
              backgroundColor: 'rgba(1,1,1,0.5)',
              borderRadius: 20,
              borderColor: '#111', alignItems: 'center',
              padding: responsiveWidth(2), justifyContent: 'center'
            }}>
              <View style={{
                width: responsiveWidth(80),
                height: responsiveHeight(5),
                flexDirection: 'row',
                borderBottomWidth: 1,
                alignItems: 'center',
                borderColor: '#fff'
              }}>
                <View style={{ flex: 0.9 }}>
                  <Text style={Styles.boldText}
                    allowFontScaling={false}>{this.state.chosenDate}</Text>
                </View>
                <View style={{ flex: 0.1, alignItems: 'flex-end' }}>
                  <TouchableOpacity onPress={this.showPicker}>
                    <Icon name="calendar" type="AntDesign" style={{ color: '#fff' }} />
                  </TouchableOpacity>

                  <DateTimePickerModal
                    isVisible={this.state.isVisible}
                    onConfirm={this.handlePicker}
                    onCancel={this.hidePicker}
                    mode={'date'}
                    is24Hour={false} />
                </View>
              </View>

              <View style={{ width: responsiveWidth(80), marginTop: responsiveWidth(9) }}>
                <Text style={Styles.boldText}
                  allowFontScaling={false}>Select Appointment Time</Text>
                <View style={{
                  marginTop: responsiveWidth(1),
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center'
                }}>

                  <TouchableOpacity style={{
                    marginHorizontal: responsiveWidth(2),
                    borderBottomWidth: 1,
                    borderColor: this.state.bar1
                  }}
                    onPress={() => { this.setState({ bar1: '#fff', bar2: '#111', bar3: '#111', bar4: '#111', chosenTime: '09:30:00' }) }}>
                    <Text style={Styles.normalText}
                      allowFontScaling={false}>09:30 AM</Text>
                  </TouchableOpacity>

                  <TouchableOpacity style={{
                    marginHorizontal: responsiveWidth(2),
                    borderBottomWidth: 1,
                    borderBottomColor: this.state.bar2
                  }}
                    onPress={() => { this.setState({ bar1: '#111', bar2: '#fff', bar3: '#111', bar4: '#111', chosenTime: '10:30:00' }) }}>
                    <Text style={Styles.normalText}
                      allowFontScaling={false}>10:30 AM</Text>
                  </TouchableOpacity>

                  <TouchableOpacity style={{
                    marginHorizontal: responsiveWidth(2),
                    borderBottomWidth: 1,
                    borderBottomColor: this.state.bar3
                  }}
                    onPress={() => { this.setState({ bar1: '#111', bar2: '#111', bar3: '#fff', bar4: '#111', chosenTime: '11:30:00' }) }}>
                    <Text style={Styles.normalText}
                      allowFontScaling={false}>11:30 AM</Text>
                  </TouchableOpacity>

                  <TouchableOpacity style={{
                    marginHorizontal: responsiveWidth(2),
                    borderBottomWidth: 1,
                    borderBottomColor: this.state.bar4
                  }}
                    onPress={() => { this.setState({ bar1: '#111', bar2: '#111', bar3: '#111', bar4: '#fff', chosenTime: '12:30:00' }) }}>
                    <Text style={Styles.normalText}
                      allowFontScaling={false}>12:30 PM</Text>
                  </TouchableOpacity>
                </View>
              </View>


              <View style={{ width: responsiveWidth(80), marginTop: responsiveWidth(9) }}>
                <Text style={Styles.boldText}
                  allowFontScaling={false}>Any messgae regard requested services (optional)</Text>
                <TextInput style={{
                  width: responsiveWidth(80),
                  height: responsiveHeight(15),
                  color: '#fff',
                  borderWidth: 1, borderColor: '#fff',borderRadius:10,
                  marginTop: responsiveWidth(2),
                  padding: responsiveWidth(1)
                }}
                  multiline={true}
                  placeholder="Start typing Here"
                  numberOfLines={5} />
              </View>
            </Card>
            <TouchableOpacity style={Styles.mainButtons} onPress={() => { this.verifyDetails() }}>
              <Text style={Styles.buttonText}
                allowFontScaling={false}>Set Appointment</Text>
            </TouchableOpacity>
          </SafeAreaView>
        </ImageBackground>
      </Container>
    );
  }
}

