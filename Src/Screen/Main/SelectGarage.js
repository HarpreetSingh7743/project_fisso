import React, { Component } from 'react';
import { Container, Icon, Content } from "native-base";
import { ImageBackground, StatusBar, TextInput, View, Text, KeyboardAvoidingView, Modal, TouchableOpacity, Platform } from 'react-native';
import { responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions';
import MapView, { PROVIDER_GOOGLE, Marker, Callout } from 'react-native-maps';
import Styles from '../../components/Styles/Main/Styles'
import SearchGarage from './SearchGarage'

import ViewGarageServicesModal from '../../components/Styles/CustomComponents/Modals/ViewGarageServicesModal'

export default class SelectGarage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchText: '',
            garageName: "",
            verified: false,
            garageEmail: "",
            garageAddress: "",
            garageOpen: "",
            garageClose: "",
            services: [],
            openDays: '',
            latitude: this.props.navigation.state.params.UserLat,
            longitude: this.props.navigation.state.params.UserLong,
            VisibleGarageServicesModal: false,
            allGarages: [
                {
                    key: 1,
                    verified: false,
                    garageName: "Honda Care",
                    garageEmail: 'hondaofficial@gmail.com',
                    garageAddress: '#123, Sector 43, Chandigarh',
                    garageOpen: "08:00 AM",
                    garageClose: "10:00 AM",
                    services: ["Regular AC service", "Car Interior polishing", "Deep all around cleaning"],
                    openDays: "Mon, Tue, Wed, Thu, fri, Sat",
                    latitude: 30.718609,
                    longitude: 76.719767
                },
                {
                    key: 2,
                    verified: false,
                    garageName: "Bajaj Care",
                    garageEmail: 'bajajofficial@gmail.com',
                    garageAddress: '#122, Sector 55, Chandigarh',
                    garageOpen: "08:00 AM",
                    garageClose: "10:00 AM",
                    services: ["Deep all around cleaning", "WindShield replacement", "Battery Replacement"],
                    openDays: "Mon, Tue, Wed, Thu, fri, Sat",
                    latitude: 30.725961,
                    longitude: 76.752159
                },
                {
                    key: 3,
                    verified: true,
                    garageName: "Bittu garage",
                    garageEmail: 'GarageBittu09@gmail.com',
                    garageAddress: '#988, Sector 36, Chandigarh',
                    garageOpen: "08:00 AM",
                    garageClose: "10:00 AM",
                    services: ["Wheel Balancing", "Insuarance", "Wheel Alignment", "Engine Checkup"],
                    openDays: "Mon, Tue, Wed, Thu, fri, Sat",
                    latitude: 30.735347,
                    longitude: 76.749923
                },
                {
                    key: 4,
                    verified: true,
                    garageName: "Harry's garage",
                    garageEmail: 'HarryGarage@gmail.com',
                    garageAddress: '#988, Sector 36, Chandigarh',
                    garageOpen: "08:00 AM",
                    garageClose: "10:00 AM",
                    services: ["Insuarance", "Wheel Alignment", "Engine Checkup", "COVID 19 Sanitaziation", "Replace Coolant"],
                    openDays: "Mon, Tue, Wed, Thu, fri, Sat",
                    latitude: 30.738164,
                    longitude: 76.773219
                }
            ]
        };
    }
    renderScreen() {
        const mapstyle = [
            {
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#242f3e"
                    }
                ]
            },
            {
                "elementType": "labels.text",
                "stylers": [
                    {
                        "color": "#ffffff"
                    }
                ]
            },
            {
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#746855"
                    }
                ]
            },
            {
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "color": "#242f3e"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative.locality",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#d59563"
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#000000"
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "labels.text",
                "stylers": [
                    {
                        "color": "#ffffff"
                    }
                ]
            },
            {
                "featureType": "poi",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#d59563"
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#263c3f"
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#6b9a76"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#38414e"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#212a37"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#9ca5b3"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#746855"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#1f2835"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#f3d19c"
                    }
                ]
            },
            {
                "featureType": "transit",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#2f3948"
                    }
                ]
            },
            {
                "featureType": "transit.station",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#d59563"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#17263c"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#515c6d"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "color": "#17263c"
                    }
                ]
            }
        ]
        if (this.state.searchText == "") {
            return (
                <View>
                    {this.state.VisibleGarageServicesModal && <ViewGarageServicesModal garageServices={this.state.services} onExit={(val) => { this.setState({ VisibleGarageServicesModal: val }) }} />}

                    <View style={{
                        width: responsiveWidth(100), height: responsiveHeight(60), alignItems: 'center', justifyContent: 'center'
                    }}>


                        <MapView style={Styles.mapview}
                            provider={PROVIDER_GOOGLE}
                            region={{
                                latitude: this.state.latitude,
                                longitude: this.state.longitude,
                                latitudeDelta: 0.0425,
                                longitudeDelta: 0.0525,
                            }}
                            zoomEnabled={true}
                            scrollEnabled={true}
                            showsUserLocation={true}
                            customMapStyle={mapstyle}>
                            {
                                this.state.allGarages.map((item) => {
                                    return (
                                        <Marker key={item.key}
                                            coordinate={{ latitude: item.latitude, longitude: item.longitude }}
                                            onPress={() => {
                                                this.setState({
                                                    garageName: item.garageName, verified: item.verified, garageEmail: item.garageEmail, garageAddress: item.garageAddress, garageOpen: item.garageOpen, garageClose: item.garageClose,
                                                    services: item.services, openDays: item.openDays, services: item.services, latitude: item.latitude, longitude: item.longitude
                                                })
                                            }}>
                                            <Callout>
                                                <Text style={{ color: '#111', fontWeight: 'bold' }}>{item.garageName}</Text>
                                                <Text>{item.garageEmail}</Text>
                                            </Callout>
                                        </Marker>
                                    )

                                })
                            }
                        </MapView>
                    </View>
                    <View style={{ width: responsiveWidth(100), height: responsiveHeight(29), alignItems: 'center', justifyContent: 'center' }}>
                        <View style={{ width: responsiveWidth(95), height: responsiveHeight(25), borderWidth: 0, borderColor: '#fff', borderRadius: 10 }}>
                            <View style={{ flex: 0.7, paddingHorizontal: responsiveWidth(3) }}>
                                <Text style={Styles.IntermediateText}
                                    allowFontScaling={false}>{this.state.garageName}</Text>

                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={Styles.boldText}
                                        allowFontScaling={false}>Address : </Text>
                                    <Text style={Styles.normalText}
                                        allowFontScaling={false}>{this.state.garageAddress}</Text>
                                </View>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={Styles.boldText}
                                        allowFontScaling={false}>Open : </Text>
                                    <Text style={Styles.normalText}
                                        allowFontScaling={false}>{this.state.garageOpen} - {this.state.garageClose}</Text>
                                </View>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={Styles.boldText}
                                        allowFontScaling={false}>Open Days : </Text>
                                    <Text style={Styles.normalText}
                                        allowFontScaling={false}>{this.state.openDays}</Text>
                                </View>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={Styles.boldText}
                                        allowFontScaling={false}>Services :   </Text>
                                    <TouchableOpacity onPress={() => { this.setState({ VisibleGarageServicesModal: true }) }}>
                                        <Text style={{ color: '#2B6DFF' }}
                                            allowFontScaling={false}>View Services</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={{ flex: 0.3, flexDirection: 'row' }}>
                                <View style={{ flex: 0.5 }}>
                                    <TouchableOpacity style={{
                                        height: responsiveHeight(5), width: responsiveWidth(20), alignItems: 'center',
                                        justifyContent: 'center', borderColor: '#fff', borderWidth: 1, borderRadius: 10
                                    }}
                                        onPress={() => { this.props.navigation.goBack() }}>
                                        <Text style={Styles.buttonText}
                                            allowFontScaling={false}>Back</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={{ flex: 0.5, alignItems: 'flex-end', justifyContent: 'center' }}>
                                    <TouchableOpacity style={{
                                        height: responsiveHeight(5), width: responsiveWidth(20), alignItems: 'center',
                                        justifyContent: 'center', borderColor: '#fff', borderWidth: 1, borderRadius: 10
                                    }}
                                        onPress={() => {
                                            if (this.state.garageName != "") {
                                                this.props.navigation.navigate("GarageServices", {
                                                    garageName: this.state.garageName,
                                                    verified:this.state.verified,
                                                    garageAddress: this.state.garageAddress,
                                                    garageEmail: this.state.garageEmail,
                                                    openDays: this.state.openDays,
                                                    garageClose: this.state.garageClose,
                                                    garageOpen: this.state.garageOpen,
                                                    garageServices: this.state.services
                                                })
                                            }
                                            else {
                                                alert("Atleast select a Nearby Garage")
                                            }
                                        }}>
                                        <Text style={Styles.buttonText}
                                            allowFontScaling={false}>Next</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
            )
        }
        else {
            return (
                <SearchGarage />
            )
        }
    }

    render() {
        console.log(this.state.latitude+" , "+this.state.longitude
        
        );
        
        return (
            <Container style={{ flex: 1 }}>
                <ImageBackground source={require('../../components/Images/BG/Back.jpg')}
                    style={{ width: responsiveWidth(100), height: responsiveHeight(104) }} resizeMode="cover">

                    <KeyboardAvoidingView behavior="padding">
                        <View style={{
                            width: responsiveWidth(100), height: responsiveHeight(10),
                            justifyContent: 'center', alignItems: Platform.OS === "android" ? "flex-end" : "center", flexDirection: 'row'
                        }}>
                            <View style={{ flexDirection: 'row', alignItems: "center" }}>
                                <Icon name="search" type="Ionicons" style={{ color: '#fff', marginRight: responsiveWidth(2) }} />
                                <TextInput style={Styles.mapsearchBarInput}
                                    placeholder="Search garage here"
                                    onChangeText={(searchText) => { this.setState({ searchText: searchText }) }} />
                            </View>
                        </View>
                        {this.renderScreen()}
                    </KeyboardAvoidingView>
                </ImageBackground>
            </Container>
        )
    }
}