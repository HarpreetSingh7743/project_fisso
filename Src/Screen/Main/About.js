import React, { Component } from 'react';
import { View, Text, ImageBackground, ScrollView, TouchableOpacity, Platform, SafeAreaView, Dimensions } from 'react-native';
import { Container, Icon } from "native-base";
import { responsiveFontSize, responsiveHeight, responsiveWidth } from "react-native-responsive-dimensions";
import Styles from '../../components/Styles/Main/Styles'
let height = Dimensions.get("screen").height
let width = Dimensions.get("screen").width
class About extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <Container>
        <ImageBackground source={require('../../components/Images/BG/About.jpg')}
          style={{ width: width, height: height }} resizeMode="cover">
          <View style={{ flex: 1 }}>
            <View style={{ flex: 0.1, justifyContent: Platform.OS == "android" ? 'flex-end' : "center" }}>
              <Text style={Styles.TitleText}
                allowFontScaling={false}>About Fisso</Text>
            </View>

            <View style={{ flex: 0.9, padding: responsiveWidth(2) }}>

              <View style={{
                width: responsiveWidth(90), height: responsiveHeight(65), backgroundColor: 'rgba(1,1,1,0.6)', alignSelf: 'center',
                borderWidth: 2, borderColor: '#fff', borderRadius: 10, padding: responsiveWidth(2)
              }}>
                <ScrollView>
                  <Text style={{ color: '#fff', marginVertical: responsiveWidth(1) }}
                    allowFontScaling={false}>1. Fisso app is a totally unique Smartphone App that brings your business closer to your customers by linking directly to them via their mobile phone. Fisso is an Italian word which means fixed.</Text>
                  <Text style={{ color: '#fff', marginVertical: responsiveWidth(1) }}
                    allowFontScaling={false}>2. FISSO gives real value to your customers by offering a genuinely useful smartphone app. The app has many features as well as huge amount of useful information about their vehicle. </Text>
                  <Text style={{ color: '#fff', marginVertical: responsiveWidth(1) }}
                    allowFontScaling={false}>3. As a business offering the fisso app, you will have a direct link to your customers and can gain real-time access to invaluable marketing data via a fully customized administration portal. </Text>
                  <Text style={{ color: '#fff', marginVertical: responsiveWidth(1) }}
                    allowFontScaling={false}>4. Fisso helps you to book their appointments online at this app. It provides basic and essentials garage services to the users. You can view and avail the services of any garage that are linked to this app. As we know that in today’s world, everything goes online which saves time and money, so with this app, we can provide garage services to the users and user can avail any services according to their needs. User can book online appointment that can save their time because they have given time slot for their services and within that time slot they can go to garage and avail the repairs and services under their time slot.</Text>
                  <Text style={{ color: '#fff', marginVertical: responsiveWidth(1) }}
                    allowFontScaling={false}>5. You can avail multiple services and view their orders in order screen. You can view spare parts according to their convenience. you can view all garages that linked with this app and can use any garage services according to their needs. This system of offering services online can save time and money so that user need not to wait for their turn in queue instead they have given particular time slot for servicing.</Text>
                </ScrollView>
              </View>
              <View style={{ flexDirection: 'row', width: responsiveWidth(100), alignItems: 'center', justifyContent: 'center', marginTop: responsiveWidth(15) }}>
                <TouchableOpacity style={Styles.socialIcons}>
                  <Icon name="facebook-square" type="FontAwesome" style={{ color: '#fff' }} />
                </TouchableOpacity>
                <TouchableOpacity style={Styles.socialIcons}>
                  <Icon name="twitter-square" type="FontAwesome" style={{ color: '#fff' }} />
                </TouchableOpacity>
                <TouchableOpacity style={Styles.socialIcons}>
                  <Icon name="instagram" type="FontAwesome" style={{ color: '#fff' }} />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </ImageBackground>
      </Container>
    );
  }
}

export default About;
