import React, { Component } from 'react';
import { View, Text, ImageBackground, TouchableOpacity, Image } from 'react-native';
import { Container, Content, Icon } from 'native-base';
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions';
import Styles from '../../components/Styles/Main/Styles';
import { ScrollView } from 'react-native-gesture-handler';

class Notification extends Component {
  bookmsg = "Dear customer Please bring your vehicle to garage with all Documents of Vehicle on 14/06/2020 at 11:00 AM"
  cancelmsg = "Dear Customer As you requested your booking has been cancelled."
  completemsg = "Dear Customer your vehicle has been serviced on 14/06/2020 at 02:00 PM.Please meet us on the reception"

  arrayView = []
  arrayContent = [["Honda City 2018", "13/05/2020", "Service Completed", "ABC Garage", "#123, Mohali", this.completemsg],
  ["Honda City 2018", "13/05/2020", "Service Booked", "ABC Garage", "#123, Mohali", this.bookmsg],
  ["Maruti Swift 2016", "10/05/2020", "Service Cancelled", "ABC Garage", "#123, Mohali", this.cancelmsg],
  ["Maruti Swift 2016", "10/05/2020", "Service Booked", "ABC Garage", "#123, Mohali", this.bookmsg],
  ]
  drawView = () => {
    this.arrayView = []
    for (let i = 0; i < this.arrayContent.length; i++) {
      this.arrayView.push(
        <View style={{
          width: responsiveWidth(95), height: responsiveHeight(28),
          borderColor: '#fff', borderWidth: 1, borderRadius: 10, padding: responsiveWidth(2),
          marginVertical: responsiveWidth(3), backgroundColor: 'rgba(1,1,1,0.5)'
        }} key={i}>
          <View style={{
            flex: 0.3, backgroundColor: 'rgba(153,153,153,0.4)', flexDirection: 'row',
            alignItems: 'center', borderRadius: 10, paddingHorizontal: responsiveWidth(2)
          }}>
            <View style={{ flex: 0.6 }}><Text style={{ color: '#fff', fontWeight: 'bold', fontSize: responsiveFontSize(2) }}>{this.arrayContent[i][0]}</Text></View>
            <View style={{ flex: 0.4, alignItems: 'flex-end' }}><Text style={{ color: '#fff' }}>{this.arrayContent[i][1]}</Text></View>
          </View>
          <View style={{ flex: 0.7 }}>
            <View style={{ flexDirection: 'row' }}>
              <Text style={Styles.boldText}
                allowFontScaling={false}>Subject  :   </Text>
              <Text style={Styles.normalText}
                allowFontScaling={false}>{this.arrayContent[i][2]}</Text>
            </View>
            <View style={{ flexDirection: 'row' }}>
              <Text style={Styles.boldText}
                allowFontScaling={false}>Garage   :   </Text>
              <Text style={Styles.normalText}
                allowFontScaling={false}>{this.arrayContent[i][3]}</Text>
            </View>
            <View style={{ flexDirection: 'row' }}>
              <Text style={Styles.boldText}
                allowFontScaling={false}>Address :   </Text>
              <Text style={Styles.normalText}
                allowFontScaling={false}>{this.arrayContent[i][4]}</Text>
            </View>
            <Text style={Styles.normalText}
              allowFontScaling={false}>{this.arrayContent[i][5]}</Text>
          </View>
        </View>
      )
    }
  }

  render() {
    this.drawView()
    return (
      <Container>
        <ImageBackground source={require('../../components/Images/BG/Back.jpg')}
          style={{ width: responsiveWidth(100), height: responsiveHeight(104) }} resizeMode="cover">

          <View style={{ width: responsiveWidth(100), height: responsiveHeight(10), borderWidth: 0, borderColor: '#fff', alignItems: 'flex-end', flexDirection: 'row' }}>
            <View style={{ alignItems: 'center', flexDirection: 'row' }}>
              <Text style={Styles.TitleText}
                allowFontScaling={false}>Notifications</Text>
            </View>
          </View>
          
          <View style={{ width: responsiveWidth(100), height: responsiveHeight(89), alignItems: 'center' }}>
            <ScrollView showsVerticalScrollIndicator={false}>
              {this.arrayView}
            </ScrollView>
          </View>

        </ImageBackground>
      </Container>
    );
  }
}

export default Notification;
