import React, { Component } from 'react';
import { View, Text, ImageBackground, TouchableOpacity, ScrollView, SafeAreaView, Image, Platform } from 'react-native';
import { Container, Content, Icon } from 'native-base';
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';
import styles from '../../components/Styles/Main/Styles';
import { CheckBox } from "native-base";

class Cart extends Component {
  state = {
    garageName: "ABC Garage",
    garageAddress: "#123, Mohali",
    garageEmail: "helloworld@gmail.com",
    verified: true,
    cartContents:this.props.navigation.state.params.selectedServices
  }
  drawView() {
    return this.state.cartContents.map(t => {
      return (
        <View key={t} style={{ height: responsiveHeight(5), width: responsiveWidth(90), marginTop: responsiveWidth(2), flexDirection: 'row' }}>
          <View style={{ flex: 0.7 }}><Text style={styles.boldText}>{t}</Text></View>
          <View style={{ flex: 0.3, alignItems: 'flex-end' }}>
            <TouchableOpacity onPress={() => { this.deleteItems(t) }}>
              <Icon name="circle-with-cross" type="Entypo" style={{ color: '#fff' }} />
            </TouchableOpacity>
          </View>
        </View>
      )
    })
  }

  deleteItems = (t) => {
    var arr = this.state.cartContents
    var pos = arr.indexOf(t);
    arr.splice(pos, 1)
    this.setState({ cartContents: arr })
  }

  clearCart() {
    var empty = this.state.cartContents
    empty.splice(0, empty.length)
    this.setState({ cartContents: empty })
  }
  verifedGarage() {
    if (this.state.verified == true) {
      return (
        <View style={{ width: responsiveWidth(10), height: responsiveHeight(5), marginLeft: responsiveWidth(3) }}>
          <Image source={{ uri: 'http://getdrawings.com/free-icon/check-icon-png-63.png' }}
            style={{ width: responsiveWidth(8), height: responsiveHeight(4.2) }} />
        </View>
      )
    }
  }

  render() {    
    return (
      <Container>
        <ImageBackground
          source={require('../../components/Images/BG/Back.jpg')}
          style={{ width: responsiveWidth(100), height: responsiveHeight(104) }} resizeMode="cover">
          <Content>
            <View style={{ width: responsiveWidth(100), height: responsiveHeight(10), justifyContent: Platform.OS == "android" ? 'flex-end' : "center" }}>
              <Text style={styles.TitleText}
                allowFontScaling={false}>Cart</Text>
            </View>

            <View style={{ width: responsiveWidth(100), height: responsiveHeight(80) }}>
              <SafeAreaView style={{ height: responsiveHeight(12), marginTop: responsiveWidth(1), marginHorizontal: responsiveWidth(1), borderWidth: 0, borderColor: '#fff' }}>
                <SafeAreaView style={{ flexDirection: 'row' }}>
                  <View style={{ flex: 0.7, flexDirection: 'row' }}>
                    <Text style={styles.headerText}
                      allowFontScaling={false}>{this.state.garageName}</Text>
                    {this.verifedGarage()}
                  </View>
                </SafeAreaView>
                <Text style={{ color: '#fff', fontWeight: 'bold' }}
                  allowFontScaling={false}>{this.state.garageAddress}</Text>
                <Text style={styles.normalText}
                  allowFontScaling={false}>{this.state.garageEmail}</Text>
              </SafeAreaView>
              <SafeAreaView style={{ height: responsiveHeight(62), width: responsiveWidth(100) }}>
                <View style={{ borderBottomWidth: 2, borderColor: '#fff', justifyContent: 'center', marginHorizontal: responsiveWidth(3) }}>
                  <Text style={styles.IntermediateText}
                    allowFontScaling={false}>Selected Services</Text>
                </View>
                <View style={{ height: responsiveHeight(62), width: responsiveWidth(100), alignItems: 'center' }}>
                  <ScrollView>
                    {this.drawView()}
                  </ScrollView>
                </View>
              </SafeAreaView>
            </View>

            <View style={{ width: responsiveWidth(100), height: responsiveHeight(8), flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
              <TouchableOpacity style={styles.mainButtons} onPress={() => {this.props.navigation.goBack() }}>
                <Text style={styles.buttonText}
                  allowFontScaling={false}>Back</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.mainButtons} onPress={() => {
                if (this.state.cartContents == "") {
                  alert("Cart Empty")
                }
                else {
                  alert("Your Request has been sent to Garage, They will tell you the Total Price for your Required Services. Thanks for using FISSO")
                  this.props.navigation.navigate("Dashboard")
                }
              }}>
                <Text style={styles.buttonText}
                  allowFontScaling={false}>Request Service</Text>
              </TouchableOpacity>
            </View>
          </Content>
        </ImageBackground>
      </Container>
    );
  }
}

export default Cart;
