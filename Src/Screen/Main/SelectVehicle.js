import React, { Component } from 'react';
import { Container, Card, CardItem, Input, Item, Content, Toast, View } from "native-base";
import { ImageBackground, StatusBar, Text, KeyboardAvoidingView } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { RFPercentage } from "react-native-responsive-fontsize";
import { Dropdown } from 'react-native-material-dropdown';
import Styles from '../../components/Styles/Main/Styles'
import year from '../../components/Data/Year/index';
import carCompanies from '../../components/Data/Companies/Car Companies/index';
import bikeCompanies from '../../components/Data/Companies/Bike Companies/index';
import data1 from '../../components/Data/Models/Cars Models/MarutiSuzuki/index';
import data2 from '../../components/Data/Models/Cars Models/Hyundai/index';
import data3 from '../../components/Data/Models/Cars Models/Honda/index';
import heromotocorp from '../../components/Data/Models/Bikes Models/Hero Moto Corp/index';
import honda from '../../components/Data/Models/Bikes Models/Honda/index';
import ktm from '../../components/Data/Models/Bikes Models/KTM/index';
import { responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions';
import { TouchableOpacity } from 'react-native-gesture-handler';
import * as Location from 'expo-location';

let models = [];
let companies = [];

var info = [{ brand: '' },
{ model: '' },
{ type: '' },
{ year: '' },
{ regnumber: '' },
{ contact: '' }
];

export default class VehicleSelection extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: this.props.navigation.state.params.selected,
            year: '',
            make: '',
            model: '',
            regnumber: '',
            mobile: '',
            UserLat: 30.732196,
            UserLong: 76.734230
        };
       this.getUserLocation()
    }

    getUserLocation = async () => {
      try{  let { status } = await Location.requestPermissionsAsync();
        console.log(status)
        if (status !== 'granted') {
            setErrorMsg('Permission to location Denied')
        }
        else {
            let location = await Location.getCurrentPositionAsync({});
            let latitude = location.coords.latitude;
            let longitude = location.coords.longitude
            this.setState({ UserLat: latitude, UserLong: longitude })
            
        }}
        catch{
            console.log("aa")
        }
    }
    onClick = () => {
        
        var { year, make, model, regnumber, mobile } = this.state;
        var IndNum = /^[0]?[6789]\d{9}$/;
        let validReg = /(([A-Za-z]){2,3}(|-)(?:[0-9]){1,2}(|-)(?:[A-Za-z]){2}(|-)([0-9]){1,4})|(([A-Za-z]){2,3}(|-)([0-9]){1,4})/;
        if (year && make && model && regnumber && mobile != undefined) {
            if (regnumber.match(validReg) || regnumber == "") {
                if (mobile.length === 10 && mobile.match(IndNum)) {
                    this.props.navigation.navigate("SelectGarage", { info: info,UserLat:this.state.UserLat,UserLong:this.state.UserLong })
                    console.log(info)
                }
                else {
                    Toast.show({
                        text: "Enter Valid Mobile",
                        textStyle: { textAlign: "center", color: '#111' },
                        duration: 2000,
                        style: { borderRadius: 10, margin: responsiveWidth(10), backgroundColor: 'rgba(255,255,255,0.9)' }
                    });
                }
            }
            else {
                Toast.show({
                    text: "Enter Registration Number in correct Format i.e(CH12AB1234)",
                    textStyle: { textAlign: "center", color: '#111' },
                    duration: 2000,
                    style: { borderRadius: 10, margin: responsiveWidth(10), backgroundColor: 'rgba(255,255,255,0.9)' }
                });
            }
        }
        else {
            Toast.show({
                text: "Please enter all details",
                textStyle: { textAlign: "center", color: '#111' },
                duration: 2000,
                style: { borderRadius: 10, margin: responsiveWidth(10), backgroundColor: 'rgba(255,255,255,0.9)' }
            });
        }
    }

    render() {
        const { navigate } = this.props.navigation
        info[0] = this.state.make;
        info[1] = this.state.model;
        info[2] = this.state.selected;
        info[3] = this.state.year;
        info[4] = this.state.regnumber;
        info[5] = this.state.mobile;

        if (this.state.selected === 'car') {
            companies = carCompanies;
            if (this.state.make === 'Maruti') {
                models = data1
            }
            if (this.state.make === 'Hyundai') {
                models = data2
            }
            if (this.state.make === 'Honda') {
                models = data3
            }
        }

        if (this.state.selected === 'bike') {
            companies = bikeCompanies;
            if (this.state.make === 'Hero Moto Corp') {
                models = heromotocorp
            }
            if (this.state.make === 'Honda') {
                models = honda
            }
            if (this.state.make === 'KTM') {
                models = ktm
            }
        }

        return (
            <Container style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <ImageBackground source={require('../../components/Images/BG/Back.jpg')}
                    style={{ width: responsiveWidth(100), height: responsiveHeight(104) }} resizeMode="cover">
                    <StatusBar translucent backgroundColor="rgba(0,0,0,0.5)" />
                    <KeyboardAvoidingView behavior={Platform.OS === "ios" ? "position" : 'padding'}>
                        <Card style={{
                            height: hp('75%'), width: wp("90%"),
                            alignSelf: 'center', marginTop: hp("20%")
                        }} transparent>
                            <CardItem style={{ backgroundColor: 'transparent', flexDirection: 'column' }} header>
                                <Text style={Styles.headerText} allowFontScaling={false}>SELECT VEHICLE</Text>
                            </CardItem>
                            <CardItem style={{ backgroundColor: 'transparent', flexDirection: 'column' }}>

                                <Dropdown
                                    label='Choose Make'
                                    baseColor='white'
                                    textColor='white'
                                    containerStyle={Styles.customDropDown}
                                    data={companies}
                                    fontSize={RFPercentage(2)}
                                    pickerStyle={{ backgroundColor: 'rgba(1,1,1,0.9)', marginTop: hp('10%') }}
                                    itemColor='white'
                                    selectedItemColor='white'
                                    disabledItemColor='white'
                                    onChangeText={(input) => this.setState({ make: input })}

                                />
                                <Dropdown
                                    label='Choose Model'
                                    baseColor='white'
                                    textColor='white'
                                    containerStyle={Styles.customDropDown}
                                    data={models}
                                    fontSize={RFPercentage(2)}
                                    pickerStyle={{ backgroundColor: 'rgba(1,1,1,0.9)', marginTop: hp('10%') }}
                                    itemColor='white'
                                    selectedItemColor='white'
                                    disabledItemColor='white'
                                    onChangeText={(input) => this.setState({ model: input })}
                                />
                                <Dropdown
                                    label='Year'
                                    baseColor='white'
                                    textColor='white'
                                    containerStyle={Styles.customDropDown}
                                    data={year}
                                    fontSize={RFPercentage(2)}
                                    pickerStyle={{ backgroundColor: 'rgba(1,1,1,0.9)', marginTop: hp('10%') }}
                                    itemColor='white'
                                    selectedItemColor='white'
                                    disabledItemColor='white'
                                    onChangeText={(input) => this.setState({ year: input })}
                                />
                                <Item style={{ marginBottom: hp("4%"), marginTop: hp("1%") }}>
                                    <Input placeholder="Registration No. e.g.(AB12BN1234)"
                                        allowFontScaling={false}
                                        placeholderTextColor="white"
                                        style={{ color: 'white' }}
                                        onChangeText={(input) => this.setState({ regnumber: input })} />
                                </Item>
                                <Item style={{ marginBottom: hp("5%"), marginTop: hp("1%") }}>
                                    <Input placeholder="Contact Number"
                                        allowFontScaling={false}
                                        placeholderTextColor="white"
                                        style={{ color: 'white' }}
                                        keyboardType="number-pad"
                                        onChangeText={(input) => this.setState({ mobile: input })} />
                                </Item>
                                <View style={{ flexDirection: 'row' }}>
                                    <TouchableOpacity style={Styles.mainButtons} onPress={() => { this.props.navigation.goBack(); }}>
                                        <Text style={Styles.buttonText}
                                            allowFontScaling={false}>Back</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={Styles.mainButtons} onPress={() => { this.onClick() }}>
                                        <Text style={Styles.buttonText}
                                            allowFontScaling={false}>Next</Text>
                                    </TouchableOpacity>
                                </View>
                            </CardItem>
                        </Card>
                    </KeyboardAvoidingView>
                </ImageBackground>
            </Container>
        )
    }
}



