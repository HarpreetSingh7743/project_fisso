import React, { Component } from 'react';
import { View, Text, ImageBackground, SafeAreaView, ScrollView, Image, TouchableOpacity, Platform } from 'react-native';
import { CheckBox } from "native-base"
import { responsiveFontSize, responsiveHeight, responsiveWidth } from "react-native-responsive-dimensions";
import styles from '../../components/Styles/Main/Styles'

class GarageServices extends Component {
  constructor(props) {
    super(props);
    this.state = {
      detailsVerified: false,
      garageName: this.props.navigation.state.params.garageName,
      garageEmail: this.props.navigation.state.params.garageEmail,
      garageAddress: this.props.navigation.state.params.garageAddress,
      garageOpen: this.props.navigation.state.params.garageOpen,
      garageClose: this.props.navigation.state.params.garageClose,
      openDays: this.props.navigation.state.params.openDays,
      distanceFromUser: "12 km",
      verified: this.props.navigation.state.params.verified,
      garageServices: this.props.navigation.state.params.garageServices,
      selectedServices: [],
      data: []
    };
  }

  componentDidMount() {
    let temp = this.state.garageServices
    let temp1 = []
    for (let i = 0; i < temp.length; i++) {
      var obj = {}
      obj = { "id": i, "key": temp[i], "checked": false }
      temp1.push(obj)
    }
    this.setState({ data: temp1 })
  }

  //checkbox input handler
  onCheckChanged(id) {
    const data = this.state.data;
    const index = data.findIndex(x => x.id === id);
    data[index].checked = !data[index].checked;
    this.setState(data);
  }

  renderItems() {
    return this.state.data.map((item, key) => {
      return (
        <TouchableOpacity key={key} style={{ flexDirection: 'row', marginTop: responsiveWidth(4) }} onPress={() => this.onCheckChanged(item.id)}>
          <View style={{ flex: 0.9 }}><Text style={styles.boldText} allowFontScaling={false}>{item.key}</Text></View>
          <View style={{ flex: 0.1 }}><CheckBox checked={item.checked} onPress={() => this.onCheckChanged(item.id)} /></View>
        </TouchableOpacity>)
    })
  }
  verifyDetails() {
    var res1 = this.state.data.map((t) => t.key)
    var res2 = this.state.data.map((t) => t.checked)
    var resultArray = []
    for (let i = 0; i < res2.length; i++) {
      if (res2[i] == true) {
        resultArray.push(res1[i])
      }
    }

    if (resultArray != "") {

      this.setState({ selectedServices: resultArray, detailsVerified: true })

    }
    else {
      alert("Atleast select One Service")
    }
  }

  handleClick() {
    if (this.state.detailsVerified == true) {
      this.props.navigation.navigate("Cart", { selectedServices: this.state.selectedServices })
    }
    else {
      alert("select Atleast one service from "+this.state.garageName)
    }
  }

  verifedGarage() {
    if (this.state.verified == true) {
      return (
        <View style={{ width: responsiveWidth(10), height: responsiveHeight(5), marginLeft: responsiveWidth(3) }}>
          <TouchableOpacity onPress={() => { alert("It is a Fiso Verified Garage") }}>
            <Image source={{ uri: 'http://getdrawings.com/free-icon/check-icon-png-63.png' }}
              style={{ width: responsiveWidth(8), height: responsiveHeight(4.2) }} />
          </TouchableOpacity>
        </View>
      )
    }
    else {
      return (
        <View style={{ width: responsiveWidth(10), height: responsiveHeight(5), marginLeft: responsiveWidth(3) }}>
          <TouchableOpacity onPress={() => { alert("This is not a Fisso Verified Garage, if you are known to this garage then you can go. We are not responsible for any inconvenience") }}>
            <Image source={{ uri: 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/e2/RedX.svg/600px-RedX.svg.png' }}
              style={{ width: responsiveWidth(8), height: responsiveHeight(4.2) }} />
          </TouchableOpacity>
        </View>
      )
    }
  }
  render() {
    return (
      <View style={{ flex: 1 }}>
        <ImageBackground source={require('../../components/Images/BG/Back.jpg')} style={{ flex: 1 }} resizeMode="stretch">
          <SafeAreaView style={{ flex: 0.90, alignItems: 'center', justifyContent: 'center' }} >

            <SafeAreaView style={{ width: responsiveWidth(100), alignItems: 'center' }}>
              <SafeAreaView style={{ width: responsiveWidth(95), height: responsiveHeight(25), borderBottomWidth: 1, borderColor: '#fff' }}>
                <SafeAreaView style={{ marginTop: Platform.OS === "android" ? responsiveWidth(8) : responsiveWidth(3), marginHorizontal: responsiveWidth(4) }}>
                  <SafeAreaView style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 0.7, flexDirection: 'row' }}>
                      <Text style={styles.headerText}
                        allowFontScaling={false}>{this.state.garageName}</Text>
                      {this.verifedGarage()}
                    </View>
                    <View style={{ flex: 0.3, alignItems: 'flex-end' }}>
                      <Text style={{ color: '#fff' }}
                        allowFontScaling={false}>{this.state.distanceFromUser}</Text>
                    </View>
                  </SafeAreaView>
                  <Text style={{ color: '#fff', fontWeight: 'bold' }}
                    allowFontScaling={false}>{this.state.garageAddress}</Text>
                  <Text style={styles.normalText}
                    allowFontScaling={false}>{this.state.garageEmail}</Text>
                  <Text style={styles.normalText}
                    allowFontScaling={false}>Open : {this.state.openDays}</Text>
                  <SafeAreaView style={{ alignSelf: 'flex-end', marginTop: responsiveWidth(7), borderWidth: 0, borderColor: '#fff', width: responsiveWidth(90), flexDirection: 'row' }}>
                    <SafeAreaView style={{ flex: 0.4 }}></SafeAreaView>
                    <SafeAreaView style={{ flex: 0.6, alignItems: 'flex-end' }}>
                      <Text style={{ color: '#fff', fontWeight: 'bold' }}
                        allowFontScaling={false}>{this.state.garageOpen} - {this.state.garageClose}</Text>
                    </SafeAreaView>
                  </SafeAreaView>
                </SafeAreaView>
              </SafeAreaView>
            </SafeAreaView>
            <View style={{ width: responsiveWidth(95), height: responsiveHeight(60) }}>
              <ScrollView showsVerticalScrollIndicator={false}>
                {this.renderItems()}
              </ScrollView>
            </View>
          </SafeAreaView>
          <SafeAreaView style={{ flex: 0.1, alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
            <TouchableOpacity style={styles.mainButtons} onPress={() => { this.props.navigation.goBack() }}>
              <Text style={styles.buttonText}
                allowFontScaling={false}>Back</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.mainButtons} onPressIn={() => { this.verifyDetails() }} onPressOut={() => { this.handleClick() }}>
              <Text style={styles.buttonText}
                allowFontScaling={false}>Add To Cart</Text>
            </TouchableOpacity>
          </SafeAreaView>
        </ImageBackground>
      </View>
    );
  }
}

export default GarageServices;
