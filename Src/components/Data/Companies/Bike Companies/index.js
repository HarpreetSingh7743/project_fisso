let bikeCompanies = [{value:'Hero Moto Corp'},
    {value:'Honda'},
    {value:'Yamaha'},
    {value:'TVS'},
    {value:'KTM'},
    {value:'Suzuki'},
    {value:'Mahindra'},
    {value:'Kawasaki'},
    {value:'Vespa'},
    {value:'Harley Davidson'},
    {value:'Royal Enfield'}
];

export default bikeCompanies