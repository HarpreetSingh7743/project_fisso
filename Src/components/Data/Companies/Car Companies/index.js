let carCompanies = [{value:'Hyundai'}, 
    {value: 'Maruti'},
    {value: 'Honda'},
    {value: 'Tata'},
    {value: 'Mahindra'},
    {value: 'Renault'},
    {value: 'Ford'},
    {value: 'Nissan'},
    {value: 'Toyota'},
];

export default carCompanies;