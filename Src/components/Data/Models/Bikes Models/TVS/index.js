let tvs = [{value : 'Apache RTR 160'},
    {value : 'Apache RTR 160 4V'},
    {value : 'Apache RR 310'},
    {value : 'Apache RTR 180'},
    {value : 'Jupiter'},
    {value : 'NTORQ 125'},
    {value : 'Zeppelin'},
    {value : 'Radeon'},
    {value : 'Scooty Pep Plus'},
    {value : 'Star City Plus'},
    {value : 'Victor'},
    {value : 'Scooty Zest'},
];

export default tvs;