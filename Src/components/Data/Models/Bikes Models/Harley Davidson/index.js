let hd = [{value : 'Street 750'},
    {value : 'Iron 883'},
    {value : 'Heritage Classic'},
    {value : 'Fat Boy'},
    {value : 'Fat Bob'},
    {value : 'Bronx'},
    {value : 'Deluxe'},
    {value : '350'},
    {value : 'LiveWire'},
    {value : 'Street Rod'},
    {value : 'Iron 1200'},
    {value : '1200 Custom'},
    {value : 'Low Rider'},
    {value : 'Street Bob'},
];

export default hd;