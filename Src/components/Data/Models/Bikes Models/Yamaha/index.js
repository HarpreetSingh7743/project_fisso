let yamaha = [{value : 'YZF R15 V3'},
    {value : 'FZ-S Fi Version 3.0'},
    {value : 'MT-15'},
    {value : 'Fascino'},
    {value : 'YZF-R1'},
    {value : 'FZ 25'},
    {value : 'Fascino 125'},
    {value : 'YZF R3'},
    {value : 'XSR 155'},
];

export default yamaha;