let vespa = [{value : 'LX 125'},
    {value : 'SXL 150'},
    {value : 'Elettrica'},
    {value : 'Notte 125'},
    {value : 'VXL 150'},
    {value : 'Elegante 150'},
    {value : 'SXL 125'},
    {value : 'Liberty 3V'},
    {value : 'Urban Club'},
    {value : 'RED 125'},
    {value : 'ZX 125'},
    {value : 'GTS Super 125'},
];

export default vespa;