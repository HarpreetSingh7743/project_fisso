let ktm = [{value : '200 Duke'},
    {value : '125 Duke'},
    {value : 'RC 200'},
    {value : 'RC 125'},
    {value : 'RC 390'},
    {value : '390 Duke'},
    {value : '390 Adventure'},
    {value : '250 Duke'},
    {value : '890 Duke R'},
    {value : 'RC 490'},
    {value : '490 Duke'},
];

export default ktm;