import { StyleSheet } from 'react-native'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions'

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { RFPercentage } from "react-native-responsive-fontsize";
  
export default StyleSheet.create({
    TitleText:{
        color:'#fff',
        fontWeight:'bold',
        fontSize:responsiveFontSize(5)
    },
    authHeadText:{
        fontSize:responsiveFontSize(5),
        color:'#fff',
        fontWeight:'bold'
    },
    headerText:{
        color:'#fff',
        fontSize:responsiveFontSize(3),
        fontWeight:'bold'
    },
    IntermediateText:{
        color:'#fff',
        fontSize:responsiveFontSize(2.5),
    },
    normalText:{
        color:'#fff'
    },
    boldText:{
        color:'#fff',
        fontWeight:'bold'
    },
    buttonText:{
        color:'#fff',
        fontWeight:'bold',
        fontSize:responsiveFontSize(2)
    },
    roundedButtons:{
        width:responsiveWidth(10.5),
        height:responsiveHeight(5.5),
        borderWidth:2,
        borderColor:'#fff',
        marginRight:responsiveWidth(6),
        borderRadius:50,
        alignItems:'center',
        justifyContent:'center'
    },
    mainButtons:{
        width:responsiveWidth(40),
        height:responsiveHeight(7),
        marginHorizontal:responsiveWidth(3),
        borderRadius:20,
        borderWidth:1,
        borderColor:'#fff',
        alignItems:'center',
        justifyContent:'center'
    },
    searchBarView:{
        width:responsiveWidth(90),
        height:responsiveHeight(6),
        borderWidth:2,
        borderColor:'#fff',
        borderRadius:20,
        flexDirection:'row'
    },
    searchBarInput:{
        width:responsiveWidth(75),
        borderBottomWidth:1,
        borderBottomColor:'#fff',
        color:'#fff'
    },
    socialIcons:{
        marginHorizontal:responsiveWidth(8)
    },
    customDropDown:{
        width : wp('80%'),
        paddingBottom : hp("3%")
    },
    customToast:{
        borderRadius:10,
        margin:responsiveWidth(10),
        backgroundColor:'rgba(153,153,153,0.8)'
    },
    mapsearchBarInput:{
        width:responsiveWidth(80),
        height:responsiveHeight(4),
        borderBottomWidth:1,
        borderBottomColor:'#fff',
        color:'#fff',
        marginBottom : ("3%"),
    },
    mapview:{
       height:responsiveHeight(57),
       width:responsiveWidth(95),
       marginHorizontal:responsiveWidth(2)
    },


})