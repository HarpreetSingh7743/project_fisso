import React from 'react';
import {StyleSheet,View } from 'react-native';
import * as ImagePicker from 'expo-image-picker';
import { Avatar } from 'react-native-elements';


export default function UserProfile() {
    let [selectedImage, setSelectedImage] = React.useState(null);
  
    let openImagePickerAsync = async () => {
      let permissionResult = await ImagePicker.requestCameraRollPermissionsAsync();
  
      if (permissionResult.granted === false) {
        alert('Permission to access camera roll is required!');
        return;
      }
  
      let pickerResult = await ImagePicker.launchImageLibraryAsync();
  
      if (pickerResult.cancelled === true) {
        return;
      }
  
      setSelectedImage({ localUri: pickerResult.uri });
    };
  
    if (selectedImage !== null) {
      return (
        <View style={styles.container}>
        <Avatar
        source={{ uri: selectedImage.localUri }}
        rounded
        size="xlarge"
        showEditButton
        onEditPress={openImagePickerAsync}/>
  
        
      </View>
      );
    }
  
    return (
        <View style={styles.container}>
        <Avatar
        source={{uri:"https://cdn1.iconfinder.com/data/icons/mix-color-4/502/Untitled-1-512.png"}}
        rounded
        size="xlarge"
        showEditButton
        onEditPress={openImagePickerAsync}/>
  
        
      </View>
    );
  }
  
  const styles = StyleSheet.create({
    container:{
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'rgba(1,1,1,0)'
    }
  });