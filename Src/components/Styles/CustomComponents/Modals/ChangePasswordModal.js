import React, { Component } from 'react';
import { View, Text, SafeAreaView, Modal, TouchableOpacity, TextInput } from 'react-native';
import { Card, Icon, Toast } from 'native-base';
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import Styles from '../../Main/Styles';

class ChangePasswordModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            oldpass: '',
            newpass: '',
            cpass: '',
            VisiblePasswordModal: true
        };
    }

    verifyDetails() {
        let { oldpass, newpass, cpass, } = this.state
        let passformat = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20}$/;

        if (oldpass == "" || newpass == "" || cpass == "") {
            Toast.show({
                text: "Please Enter all Fields",
                textStyle: { textAlign: "center", color: '#111' },
                duration: 4000,
                position: 'bottom',
                style: { borderRadius: 20, margin: responsiveWidth(10), backgroundColor: 'rgba(255,255,255,1)' }
            });
        }
        else {
            if (newpass.match(passformat)) {
                if (cpass == newpass) {
                    if (oldpass.match(passformat)) {

                    }
                    else {
                        Toast.show({
                            text: "Old password is wrong",
                            textStyle: { textAlign: "center", color: '#111' },
                            duration: 2000,
                            position: 'bottom',
                            style: { borderRadius: 20, margin: responsiveWidth(10), backgroundColor: 'rgba(255,255,255,1)' }
                        });
                    }
                }
                else {
                    Toast.show({
                        text: "Password didn't match",
                        textStyle: { textAlign: "center", color: '#111' },
                        duration: 2000,
                        position: 'bottom',
                        style: { borderRadius: 20, margin: responsiveWidth(10), backgroundColor: 'rgba(255,255,255,1)' }
                    });
                }

            }
            else {
                Toast.show({
                    text: "Password length must be more then 8 and a combination of Capital,Small leters and Numbers",
                    textStyle: { textAlign: "center", color: '#111' },
                    duration: 2000,
                    position: 'bottom',
                    style: { borderRadius: 20, margin: responsiveWidth(10), backgroundColor: 'rgba(255,255,255,0.8)' }
                });
            }
        }
    }
    render() {
        return (
            <Modal visible={this.state.VisiblePasswordModal} transparent={true} animationType="slide">
                <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1, backgroundColor: 'rgba(1,1,1,0.5)' }}>
                    <Card style={{
                        height: responsiveHeight(50),
                        width: responsiveWidth(80),
                        borderRadius: 20,
                        backgroundColor: 'rgba(1,1,1,0.9)'
                    }}>
                        <View style={{ flex: 0.1, flexDirection: 'row', alignItems: 'center' }}>
                            <View style={{ flex: 0.8, paddingHorizontal: responsiveWidth(2) }}>
                                <Text style={Styles.IntermediateText}>Change Password</Text>
                            </View>
                            <View style={{ flex: 0.2, alignItems: 'flex-end', padding: responsiveWidth(2) }}>
                                <TouchableOpacity onPress={()=>{this.props.onExit(false)}}>
                                    <Icon name="closecircleo" type="AntDesign" style={{ color: '#fff' }} />
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{ flex: 0.6, alignItems: 'center', justifyContent: 'center' }}>
                            <View style={{
                                flexDirection: 'row', margin: responsiveWidth(3),
                                borderRadius: 10, borderWidth: 2, borderColor: '#fff',
                                padding: responsiveWidth(2), alignItems: 'center', width: responsiveWidth(70)
                            }}>
                                <Icon name="lock" type="Feather" style={{ color: '#fff' }} />
                                <TextInput style={{
                                    width: responsiveWidth(57), borderBottomWidth: 1,
                                    borderColor: '#fff', marginLeft: responsiveWidth(1), color: '#fff'
                                }}
                                    placeholder="Enter Old Password"
                                    placeholderTextColor="#fff"
                                    secureTextEntry={true}
                                    onChangeText={(oldpass) => this.setState({ oldpass: oldpass })}
                                    allowFontScaling={false}
                                    autoCorrect={false}
                                    keyboardType="default"
                                    returnKeyType="next"
                                    onSubmitEditing={() => { this.refs.npass.focus() }} />
                            </View>
                            <View style={{
                                flexDirection: 'row', margin: responsiveWidth(3),
                                borderRadius: 10, borderWidth: 2, borderColor: '#fff',
                                padding: responsiveWidth(2), alignItems: 'center', width: responsiveWidth(70)
                            }}>
                                <Icon name="unlock" type="Feather" style={{ color: '#fff' }} />
                                <TextInput style={{
                                    width: responsiveWidth(57), borderBottomWidth: 1,
                                    borderColor: '#fff', marginLeft: responsiveWidth(1), color: '#fff'
                                }}
                                    placeholder="Enter New Password"
                                    placeholderTextColor="#fff"
                                    secureTextEntry={true}
                                    onChangeText={(newpass) => this.setState({ newpass: newpass })}
                                    allowFontScaling={false}
                                    autoCorrect={false}
                                    keyboardType="default"
                                    returnKeyType="next"
                                    ref={'npass'}
                                    onSubmitEditing={() => { this.refs.cpass.focus() }} />
                            </View>
                            <View style={{
                                flexDirection: 'row', margin: responsiveWidth(3),
                                borderRadius: 10, borderWidth: 2, borderColor: '#fff',
                                padding: responsiveWidth(2), alignItems: 'center', width: responsiveWidth(70)
                            }}>
                                <Icon name="unlock" type="Feather" style={{ color: '#fff' }} />
                                <TextInput style={{
                                    width: responsiveWidth(57), borderBottomWidth: 1,
                                    borderColor: '#fff', marginLeft: responsiveWidth(1), color: '#fff'
                                }}
                                    placeholder="Confirm New Password"
                                    placeholderTextColor="#fff"
                                    secureTextEntry={true}
                                    onChangeText={(cpass) => this.setState({ cpass: cpass })}
                                    allowFontScaling={false}
                                    autoCorrect={false}
                                    keyboardType="default"
                                    returnKeyType="go"
                                    ref={'cpass'}
                                    onSubmitEditing={() => { this.verifyDetails() }} />
                            </View>
                        </View>
                        <View style={{ flex: 0.3, alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
                            <TouchableOpacity style={Styles.mainButtons} onPress={() => { this.verifyDetails() }}>
                                <Text style={Styles.buttonText}>Confirm</Text>
                            </TouchableOpacity>
                        </View>

                    </Card>
                </View>
            </Modal>
        );
    }
}

export default ChangePasswordModal;
