import React, { Component } from 'react';
import { View, Text, Modal ,TouchableOpacity,ScrollView} from 'react-native';
import { Card,Icon } from 'native-base';
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import Styles from '../../Main/Styles';

class ViewGarageServicesModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            visibleModal:true,
            services:this.props.garageServices
        };
    }

    showServices() {
        return this.state.services.map(t => {
            return (
                <View key={t} style={{ height: responsiveHeight(5), width: responsiveWidth(90), marginTop: responsiveWidth(2), flexDirection: 'row' }}>
                    <View style={{ flex: 0.7 }}><Text style={Styles.boldText}>{t}</Text></View>
                    <View style={{ flex: 0.3, alignItems: 'flex-end' }}>
                    </View>
                </View>
            )
        })
    }
    render() {
        return (
            <Modal visible={this.state.visibleModal} transparent={true}>
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgba(1,1,1,0.9)' }}>
                <Card style={{
                    height: responsiveHeight(80),
                    width: responsiveWidth(90),
                    borderRadius: 20,
                    backgroundColor: 'rgba(1,1,1,0.9)',
                }}>
                    <View style={{ margin: responsiveWidth(3),flexDirection:'row' }}>
                       <View style={{flex:0.7}}>
                       <Text style={Styles.headerText}> Garage Services</Text>
                       </View>
                       <View style={{flex:0.3,alignItems:'flex-end',paddingHorizontal:responsiveWidth(1)}}>
                           <TouchableOpacity onPress={()=>{this.props.onExit(false)}}>
                               <Icon  name="closecircleo" type="AntDesign" style={{color:'#fff'}}/>
                           </TouchableOpacity>
                       </View>
                    </View>
                    <View style={{ margin: responsiveWidth(3), height: responsiveHeight(68) }}>
                        <ScrollView>
                            {this.showServices()}
                        </ScrollView>
                    </View>

                </Card>
            </View>
            </Modal>
        );
    }
}

export default ViewGarageServicesModal;
