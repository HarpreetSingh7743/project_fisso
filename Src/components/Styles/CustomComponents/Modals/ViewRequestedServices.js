import React, { Component } from 'react';
import { View, Text, Modal, Dimensions, TouchableOpacity, ScrollView } from 'react-native';
import { responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions';
import Styles from '../../Main/Styles';
import { Icon } from 'native-base';

let height = Dimensions.get("screen").height
let width = Dimensions.get("screen").width

class ViewRequestedServices extends Component {
    constructor(props) {
        super(props);
        this.state = {
            visibleModal: true,
            servicesDetails: this.props.servicesDetails,
            TotalBill: 0
        };
    }
    componentDidMount() {
        let TempArr = this.state.servicesDetails
        let Temp = [], total = 0
        for (let i = 0; i < TempArr.length; i++) {
            Temp.push(TempArr[i].Price)
        }
        for (let i = 0; i < Temp.length; i++) {
            total = total + Temp[i]
        }
        this.setState({ TotalBill: total })
    }
    renderItems() {
        return this.state.servicesDetails.map((t) => {
            return (
                <View key={t.ServiceName} style={{ marginTop: responsiveWidth(2), flexDirection: 'row' }}>
                    <View style={{ flex: 0.8 }}>
                        <Text style={Styles.boldText}>{t.ServiceName}</Text>
                    </View>
                    <View style={{ flex: 0.2 }}>
                        <Text style={{ color: '#fff' }}> Rs. {t.Price}</Text>
                    </View>
                </View>
            )
        })
    }
    render() {
        return (
            <Modal visible={this.state.visibleModal} transparent={true}>
                <View style={{ width: width, height: height, backgroundColor: 'rgba(1,1,1,0.4)', alignItems: 'center', justifyContent: 'center', }}>
                    <View style={{ width: responsiveWidth(95), height: responsiveHeight(70), backgroundColor: '#000', padding: responsiveWidth(2), borderWidth: 0.2, borderColor: '#fff', borderRadius: 10 }}>
                        <View style={{ flex: 0.1, flexDirection: 'row' }}>
                            <View style={{ flex: 0.8 }}>
                                <Text style={Styles.IntermediateText}>Requested Services</Text>
                            </View>
                            <View style={{ flex: 0.2, alignItems: 'flex-end' }}>
                                <TouchableOpacity onPress={() => { this.props.onExit(false) }}>
                                    <Icon name="cross" type="Entypo" style={{ color: '#fff' }} />
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{ flex: 0.8, padding: responsiveWidth(2) }}>
                            <ScrollView>
                                {this.renderItems()}
                            </ScrollView>
                        </View>
                        <View style={{ flex: 0.1, flexDirection: 'row' }}>
                            <View style={{ flex: 0.6, justifyContent: 'center', paddingHorizontal: responsiveWidth(3) }}>
                                <Text style={{ color: '#fff' }}>Number of Services: {this.state.servicesDetails.length}</Text>
                            </View>
                            <View style={{ flex: 0.4, alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={Styles.normalText}>Total Bill: {this.state.TotalBill}</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </Modal>
        );
    }
}

export default ViewRequestedServices;
