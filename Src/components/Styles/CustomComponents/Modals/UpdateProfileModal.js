import React, { Component } from 'react';
import { View, Text, SafeAreaView, Modal, TouchableOpacity, TextInput } from 'react-native';
import { Card, Icon, Toast } from 'native-base';
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import Styles from '../../Main/Styles';

class UpdateProfileModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userName: '',
            userMobile: '',
            userAddress: '',
            visibleProfileModal:true
        };
    }

    verifyDetails() {
        var { userName, userMobile, userAddress } = this.state
        var IndNum = /^[0]?[6789]\d{9}$/;
        if (userName == "" || userMobile == "" || userAddress == "") {
            Toast.show({
                text: "Please Enter all fields",
                textStyle: { textAlign: "center", color: '#111' },
                duration: 2000,
                position: 'bottom',
                style: { borderRadius: 20, margin: responsiveWidth(10), backgroundColor: 'rgba(255,255,255,1)' }
            })
        }
        else {
            if (userName.length < 3) {
                Toast.show({
                    text: "Please Enter Full Name",
                    textStyle: { textAlign: "center", color: '#111' },
                    duration: 2000,
                    position: 'bottom',
                    style: { borderRadius: 20, margin: responsiveWidth(10), backgroundColor: 'rgba(255,255,255,1)' }
                })
            }
            else {
                if (userMobile.match(IndNum)) {
                   //Enter your code here for Database

                   
                }
                Toast.show({
                    text: "Please Enter correct Mobile Number",
                    textStyle: { textAlign: "center", color: '#111' },
                    duration: 2000,
                    position: 'bottom',
                    style: { borderRadius: 20, margin: responsiveWidth(10), backgroundColor: 'rgba(255,255,255,1)' }
                })
            }
        }
    }
    render() {
        return (
            <Modal visible={this.state.visibleProfileModal} transparent={true} animationType="slide">
                <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1, backgroundColor: 'rgba(1,1,1,0.5)' }}>
                    <Card style={{
                        height: responsiveHeight(50),
                        width: responsiveWidth(80),
                        borderRadius: 20,
                        backgroundColor: 'rgba(1,1,1,0.9)'}}>
                        <View style={{ flex: 0.1, flexDirection: 'row', alignItems: 'center' }}>
                            <View style={{ flex: 0.8, paddingHorizontal: responsiveWidth(2) }}>
                                <Text style={Styles.IntermediateText}>Update Profile</Text>
                            </View>
                            <View style={{ flex: 0.2, alignItems: 'flex-end', padding: responsiveWidth(2) }}>
                                <TouchableOpacity onPress={()=>{this.props.onExit(false)}}>
                                    <Icon name="closecircleo" type="AntDesign" style={{ color: '#fff' }} />
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{ flex: 0.6, alignItems: 'center', justifyContent: 'center' }}>
                            <View style={{
                                flexDirection: 'row', margin: responsiveWidth(3),
                                borderRadius: 10, borderWidth: 2, borderColor: '#fff',
                                padding: responsiveWidth(2), alignItems: 'center', width: responsiveWidth(70)
                            }}>
                                <Icon name="user" type="SimpleLineIcons" style={{ color: '#fff' }} />
                                <TextInput style={{
                                    width: responsiveWidth(57), borderBottomWidth: 1,
                                    borderColor: '#fff', marginLeft: responsiveWidth(1), color: '#fff'
                                }}
                                    placeholder="Enter Full Name"
                                    placeholderTextColor="#fff"
                                    onChangeText={(userName) => this.setState({ userName: userName })}
                                    allowFontScaling={false}
                                    autoCorrect={false}
                                    keyboardType="default"
                                    returnKeyType="next"
                                    onSubmitEditing={() => { this.refs.npass.focus() }} />
                            </View>
                            <View style={{
                                flexDirection: 'row', margin: responsiveWidth(3),
                                borderRadius: 10, borderWidth: 2, borderColor: '#fff',
                                padding: responsiveWidth(2), alignItems: 'center', width: responsiveWidth(70)
                            }}>
                                <Icon name="phone" type="AntDesign" style={{ color: '#fff' }} />
                                <TextInput style={{
                                    width: responsiveWidth(57), borderBottomWidth: 1,
                                    borderColor: '#fff', marginLeft: responsiveWidth(1), color: '#fff'
                                }}
                                    placeholder="Enter Mobile"
                                    placeholderTextColor="#fff"
                                    onChangeText={(userMobile) => this.setState({ userMobile: userMobile })}
                                    allowFontScaling={false}
                                    autoCorrect={false}
                                    keyboardType="default"
                                    returnKeyType="next"
                                    ref={'npass'}
                                    onSubmitEditing={() => { this.refs.cpass.focus() }} />
                            </View>
                            <View style={{
                                flexDirection: 'row', margin: responsiveWidth(3),
                                borderRadius: 10, borderWidth: 2, borderColor: '#fff',
                                padding: responsiveWidth(2), alignItems: 'center', width: responsiveWidth(70)
                            }}>
                                <Icon name="home" type="AntDesign" style={{ color: '#fff' }} />
                                <TextInput style={{
                                    width: responsiveWidth(57), borderBottomWidth: 1,
                                    borderColor: '#fff', marginLeft: responsiveWidth(1), color: '#fff'
                                }}
                                    placeholder="Change Address"
                                    placeholderTextColor="#fff"
                                    onChangeText={(userAddress) => this.setState({ userAddress: userAddress })}
                                    allowFontScaling={false}
                                    autoCorrect={false}
                                    keyboardType="default"
                                    returnKeyType="go"
                                    ref={'cpass'}
                                    onSubmitEditing={() => { this.verifyDetails() }} />
                            </View>
                        </View>
                        <View style={{ flex: 0.3, alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
                            <TouchableOpacity style={Styles.mainButtons} onPress={() => { this.verifyDetails() }}>
                                <Text style={Styles.buttonText}>Update</Text>
                            </TouchableOpacity>
                        </View>

                    </Card>
                </View>
            </Modal>
        );
    }
}

export default UpdateProfileModal;
