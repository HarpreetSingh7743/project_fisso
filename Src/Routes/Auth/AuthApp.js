import {createAppContainer}from 'react-navigation';
import { createStackNavigator } from "react-navigation-stack";
import SignIn from '../../Screen/Auth/SignIn';
import SignUp from '../../Screen/Auth/SignUp';
import Forgot from '../../Screen/Auth/Forgot'
import WelcomeAuth from '../../Screen/Auth/WelcomeAuth'
const AuthAppNav=createStackNavigator({
    WelcomeAuth:WelcomeAuth,
    SignIn:SignIn,
    SignUp:SignUp,
    Forgot:Forgot
  }
  ,
  {
    initialRouteName:"WelcomeAuth",
    headerMode:"null",
    defaultNavigationOptions:{
      headerStyle:{
          
        backgroundColor:'#182',
        
      }
    }
  });
  export default createAppContainer(AuthAppNav);
  