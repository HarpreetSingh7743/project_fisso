import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";

import Dashboard from '../../Screen/Main/Dashboard'
import SelectVehicle from '../../Screen/Main/SelectVehicle'
import SearchGarage from '../../Screen/Main/SearchGarage'
import SelectGarage from '../../Screen/Main/SelectGarage'
import GarageServices from '../../Screen/Main/GarageServices'
import Cart from '../../Screen/Main/Cart'
import Requests from '../../Screen/Main/Requests'
import Notification from '../../Screen/Main/Notification'
import Profile from '../../Screen/Main/Profile'

const MainAppNav =createStackNavigator({
Dashboard:Dashboard,
SelectVehicle:SelectVehicle,
SearchGarage:SearchGarage,
SelectGarage:SelectGarage,
GarageServices:GarageServices,
Cart:Cart,
Requests:Requests,
Notification:Notification,
Profile:Profile
},{
initialRouteName:"Dashboard",
headerMode:"null"
});

export default createAppContainer(MainAppNav)