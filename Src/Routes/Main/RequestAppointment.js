import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import Appointments from '../../Screen/Main/Appointments'
import Requests from '../../Screen/Main/Requests'

const RequestAppointment =createStackNavigator({
    Appointments:Appointments,
    Requests:Requests
},{
initialRouteName:'Requests',
headerMode:'null'
})
export default createAppContainer(RequestAppointment)