import React from 'react';
import { Text, View } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';

import MainAppNav from '../Main/MainAppNav'
import RequestAppointment from '../Main/RequestAppointment'
import About from '../../Screen/Main/About'
import { Icon } from 'native-base';

const MainTabNav = createBottomTabNavigator({
    MainAppNav: {
        screen: MainAppNav,
        navigationOptions: {
            title: "Dashboard",
            tabBarIcon: () => {
                return (<Icon name="home" type="MaterialCommunityIcons" style={{color:'#fff'}}/>)
            }
        }
    },
    RequestAppointment: {
        screen: RequestAppointment,
        navigationOptions: {
            title: "Requests",
            tabBarIcon: () => {
                return (<Icon name="chat"  type="Entypo" style={{color:'#fff'}}/>)
            }
        }
    },
    About: {
        screen: About,
        navigationOptions: {
            title: "About",
            tabBarIcon: () => {
                return (<Icon name="questioncircle" type="AntDesign"  style={{color:'#fff'}}/>)
            }
        }
    },
}, {
    initialRouteName:'MainAppNav',
    tabBarOptions:{
        activeBackgroundColor:'#000',
        inactiveBackgroundColor:'#000'
    }
    

})
export default createAppContainer(MainTabNav)