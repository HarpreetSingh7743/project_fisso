import React from 'react';

import Splash from './Src/Screen/Splash/Splash'

import SignIn from './Src/Screen/Auth/SignIn'
import SignUp from './Src/Screen/Auth/SignUp'
import Forgot from './Src/Screen/Auth/Forgot'

import Dashboard from './Src/Screen/Main/Dashboard'
import VehicleSelection from './Src/Screen/Main/SelectVehicle'
import SelectGarage from './Src/Screen/Main/SelectGarage'
import GarageServices from './Src/Screen/Main/GarageServices'
import Cart from './Src/Screen/Main/Cart'
import Requests from './Src/Screen/Main/Requests'
import Appointments from './Src/Screen/Main/Appointments'
import Notification from './Src/Screen/Main/Notification'
import About from './Src/Screen/Main/About'
import Profile from './Src/Screen/Main/Profile'

import AuthAppNav from './Src/Routes/Auth/AuthApp'
import MainTabNav from './Src/Routes/MainTabNav/MainTabNav'
import { Root } from 'native-base';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { View } from 'react-native';

export default class App extends React.Component {
  state = {
    CurrentScreen: "Splash"
  }
  componentDidMount() {
    setTimeout(()=>{this.setState({CurrentScreen:"auth"})},4000)
  }
  renderScreen() {
    if (this.state.CurrentScreen == "Splash") {
      return (
        <View style={{ flex: 1 }}>
          <Splash />
        </View>
      )
    }
    else {
      return (
        <View style={{ flex: 1 }}>
          <AuthAppNav />
        </View>
      )
    }
  }
  render() {
    console.disableYellowBox = true;
    return (
      <Root>
        <SafeAreaProvider>
          <MainTabNav/>
        </SafeAreaProvider>
      </Root>
    )
  }
}